<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AlignWinClass</name>
    <message>
        <location filename="alignwin.ui" line="20"/>
        <location filename="ui_alignwin.h" line="139"/>
        <source>Pull Open</source>
        <translation type="unfinished">拉开操作</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="36"/>
        <location filename="ui_alignwin.h" line="140"/>
        <source>Pull Open Line Set</source>
        <translation type="unfinished">拉开行设置</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="42"/>
        <location filename="ui_alignwin.h" line="141"/>
        <source>Pull out the section with insufficient comparison results.</source>
        <translation type="unfinished">拉开对比结果不满意的部分行</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="51"/>
        <location filename="ui_alignwin.h" line="142"/>
        <source>Start LineNum:</source>
        <translation type="unfinished">开始行：</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="65"/>
        <location filename="ui_alignwin.h" line="143"/>
        <source>End LineNum:</source>
        <translation type="unfinished">结束行：</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="99"/>
        <location filename="ui_alignwin.h" line="144"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="119"/>
        <location filename="ui_alignwin.h" line="145"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>CCNotePad</name>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="90"/>
        <location filename="ui_ccnotepad.h" line="1183"/>
        <source>File</source>
        <translation type="unfinished">文件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="105"/>
        <location filename="ui_ccnotepad.h" line="1184"/>
        <source>Edit</source>
        <translation type="unfinished">编辑</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="109"/>
        <location filename="ui_ccnotepad.h" line="1185"/>
        <source>Format Conversion</source>
        <translation type="unfinished">格式转换</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="140"/>
        <location filename="ui_ccnotepad.h" line="1187"/>
        <source>Search</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="148"/>
        <location filename="ui_ccnotepad.h" line="1188"/>
        <source>View</source>
        <translation type="unfinished">视图</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="152"/>
        <location filename="ui_ccnotepad.h" line="1189"/>
        <source>Display symbols</source>
        <translation type="unfinished">显示符号</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="164"/>
        <location filename="ui_ccnotepad.h" line="1190"/>
        <source>Encoding</source>
        <translation type="unfinished">编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="181"/>
        <location filename="cceditor/ccnotepad.ui" line="400"/>
        <location filename="ui_ccnotepad.h" line="1191"/>
        <location filename="ui_ccnotepad.h" line="1213"/>
        <source>Language</source>
        <translation type="unfinished">语言</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="185"/>
        <location filename="ui_ccnotepad.h" line="1192"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="200"/>
        <location filename="cceditor/ccnotepad.ui" line="769"/>
        <location filename="ui_ccnotepad.h" line="1078"/>
        <location filename="ui_ccnotepad.h" line="1193"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="213"/>
        <location filename="ui_ccnotepad.h" line="1194"/>
        <source>J</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="222"/>
        <location filename="cceditor/ccnotepad.ui" line="737"/>
        <location filename="ui_ccnotepad.h" line="1074"/>
        <location filename="ui_ccnotepad.h" line="1195"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="232"/>
        <location filename="ui_ccnotepad.h" line="1196"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="238"/>
        <location filename="ui_ccnotepad.h" line="1197"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="247"/>
        <location filename="ui_ccnotepad.h" line="1198"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="256"/>
        <location filename="ui_ccnotepad.h" line="1199"/>
        <source>I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="264"/>
        <location filename="ui_ccnotepad.h" line="1200"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="273"/>
        <location filename="ui_ccnotepad.h" line="1201"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="285"/>
        <location filename="ui_ccnotepad.h" line="1202"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="297"/>
        <location filename="ui_ccnotepad.h" line="1203"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="306"/>
        <location filename="ui_ccnotepad.h" line="1204"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="314"/>
        <location filename="ui_ccnotepad.h" line="1205"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="323"/>
        <location filename="ui_ccnotepad.h" line="1206"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="332"/>
        <location filename="cceditor/ccnotepad.ui" line="998"/>
        <location filename="cceditor/ccnotepad.ui" line="1347"/>
        <location filename="ui_ccnotepad.h" line="1107"/>
        <location filename="ui_ccnotepad.h" line="1151"/>
        <location filename="ui_ccnotepad.h" line="1207"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="339"/>
        <location filename="ui_ccnotepad.h" line="1208"/>
        <source>O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="346"/>
        <location filename="ui_ccnotepad.h" line="1209"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="354"/>
        <location filename="ui_ccnotepad.h" line="1210"/>
        <source>G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="383"/>
        <location filename="ui_ccnotepad.h" line="1211"/>
        <source>Set</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="387"/>
        <location filename="ui_ccnotepad.h" line="1212"/>
        <source>Style</source>
        <translation type="unfinished">皮肤风格</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="420"/>
        <location filename="ui_ccnotepad.h" line="1215"/>
        <source>Compare</source>
        <translation type="unfinished">对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="428"/>
        <location filename="ui_ccnotepad.h" line="1216"/>
        <source>Recently</source>
        <translation type="unfinished">最近对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="432"/>
        <location filename="ui_ccnotepad.h" line="1217"/>
        <source>Dir ...</source>
        <translation type="unfinished">目录...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="437"/>
        <location filename="ui_ccnotepad.h" line="1218"/>
        <source>File ...</source>
        <translation type="unfinished">文件...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="471"/>
        <location filename="ui_ccnotepad.h" line="989"/>
        <source>New</source>
        <translation type="unfinished">新建</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="474"/>
        <location filename="ui_ccnotepad.h" line="991"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="479"/>
        <location filename="ui_ccnotepad.h" line="993"/>
        <source>Open ...</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="482"/>
        <location filename="ui_ccnotepad.h" line="995"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="487"/>
        <location filename="ui_ccnotepad.h" line="997"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="490"/>
        <location filename="ui_ccnotepad.h" line="999"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="495"/>
        <location filename="ui_ccnotepad.h" line="1001"/>
        <source>Save As ...</source>
        <translation type="unfinished">另存为</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="498"/>
        <location filename="ui_ccnotepad.h" line="1003"/>
        <source>Ctrl+Alt+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="503"/>
        <location filename="cceditor/ccnotepad.cpp" line="1649"/>
        <location filename="cceditor/ccnotepad.cpp" line="1988"/>
        <location filename="ui_ccnotepad.h" line="1005"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="506"/>
        <location filename="ui_ccnotepad.h" line="1007"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="511"/>
        <location filename="ui_ccnotepad.h" line="1009"/>
        <source>Exit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="514"/>
        <location filename="ui_ccnotepad.h" line="1011"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="519"/>
        <location filename="cceditor/ccnotepad.cpp" line="1656"/>
        <location filename="ui_ccnotepad.h" line="1013"/>
        <source>Close All</source>
        <translation type="unfinished">关闭所有</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="522"/>
        <location filename="ui_ccnotepad.h" line="1015"/>
        <source>Ctrl+Shift+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="527"/>
        <location filename="cceditor/ccnotepad.cpp" line="1688"/>
        <location filename="ui_ccnotepad.h" line="1017"/>
        <source>Undo</source>
        <translation type="unfinished">撤销</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="530"/>
        <location filename="ui_ccnotepad.h" line="1019"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="535"/>
        <location filename="cceditor/ccnotepad.cpp" line="1695"/>
        <location filename="ui_ccnotepad.h" line="1021"/>
        <source>Redo</source>
        <translation type="unfinished">重做</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="538"/>
        <location filename="ui_ccnotepad.h" line="1023"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="543"/>
        <location filename="cceditor/ccnotepad.cpp" line="1665"/>
        <location filename="ui_ccnotepad.h" line="1025"/>
        <source>Cut</source>
        <translation type="unfinished">剪切</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="546"/>
        <location filename="ui_ccnotepad.h" line="1027"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="551"/>
        <location filename="cceditor/ccnotepad.cpp" line="1672"/>
        <location filename="ui_ccnotepad.h" line="1029"/>
        <source>Copy</source>
        <translation type="unfinished">拷贝</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="554"/>
        <location filename="ui_ccnotepad.h" line="1031"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="559"/>
        <location filename="cceditor/ccnotepad.cpp" line="1679"/>
        <location filename="ui_ccnotepad.h" line="1033"/>
        <source>Paste</source>
        <translation type="unfinished">粘贴</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="562"/>
        <location filename="ui_ccnotepad.h" line="1035"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="567"/>
        <location filename="ui_ccnotepad.h" line="1037"/>
        <source>Select All</source>
        <translation type="unfinished">全选</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="570"/>
        <location filename="ui_ccnotepad.h" line="1039"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="578"/>
        <location filename="ui_ccnotepad.h" line="1041"/>
        <source>Windows(CR+LF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="586"/>
        <location filename="ui_ccnotepad.h" line="1042"/>
        <source>Unix(LF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="594"/>
        <location filename="ui_ccnotepad.h" line="1043"/>
        <source>Mac(CR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="599"/>
        <location filename="cceditor/ccnotepad.cpp" line="1704"/>
        <location filename="ui_ccnotepad.h" line="1044"/>
        <source>Find</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="602"/>
        <location filename="ui_ccnotepad.h" line="1046"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="607"/>
        <location filename="cceditor/ccnotepad.cpp" line="1711"/>
        <location filename="ui_ccnotepad.h" line="1048"/>
        <source>Replace</source>
        <translation type="unfinished">替换</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="610"/>
        <location filename="ui_ccnotepad.h" line="1050"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="615"/>
        <location filename="ui_ccnotepad.h" line="1052"/>
        <source>Go line</source>
        <translation type="unfinished">跳转</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="618"/>
        <location filename="ui_ccnotepad.h" line="1054"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="626"/>
        <location filename="ui_ccnotepad.h" line="1056"/>
        <source>Show spaces/tabs</source>
        <translation type="unfinished">显示空格</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="634"/>
        <location filename="ui_ccnotepad.h" line="1057"/>
        <source>Show end of line</source>
        <translation type="unfinished">显示行尾</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="642"/>
        <location filename="ui_ccnotepad.h" line="1058"/>
        <source>Show all</source>
        <translation type="unfinished">显示所有</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="650"/>
        <location filename="ui_ccnotepad.h" line="1059"/>
        <source>Encode in GBK</source>
        <translation type="unfinished">编码 GBK</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="658"/>
        <location filename="ui_ccnotepad.h" line="1060"/>
        <source>Encode in UTF8</source>
        <translation type="unfinished">编码 UTF8</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="666"/>
        <location filename="ui_ccnotepad.h" line="1061"/>
        <source>Encode in UTF8-BOM</source>
        <translation type="unfinished">编码 UTF8-BOM</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="674"/>
        <location filename="ui_ccnotepad.h" line="1062"/>
        <source>Encode in UCS-2 BE BOM</source>
        <translation type="unfinished">编码 UCS-2 BE BOM</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="682"/>
        <location filename="ui_ccnotepad.h" line="1063"/>
        <source>Encode in UCS-2 LE BOM</source>
        <translation type="unfinished">编码 UCS-2 LE BOM</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="687"/>
        <location filename="ui_ccnotepad.h" line="1064"/>
        <source>Convert to GBK</source>
        <translation type="unfinished">转换为 GBK 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="692"/>
        <location filename="ui_ccnotepad.h" line="1065"/>
        <source>Convert to UTF8</source>
        <translation type="unfinished">转换为 UTF8 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="697"/>
        <location filename="ui_ccnotepad.h" line="1066"/>
        <source>Convert to UTF8-BOM</source>
        <translation type="unfinished">转换为 UTF8-BOM 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="702"/>
        <location filename="ui_ccnotepad.h" line="1067"/>
        <source>Convert to UCS-2 BE BOM</source>
        <translation type="unfinished">转换为 UCS-2 BE BOM 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="707"/>
        <location filename="ui_ccnotepad.h" line="1068"/>
        <source>Convert to UCS-2 LE BOM</source>
        <translation type="unfinished">转换为 UCS-2 LE BOM 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="712"/>
        <location filename="ui_ccnotepad.h" line="1069"/>
        <source>Batch convert</source>
        <translation type="unfinished">批量转换编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="717"/>
        <location filename="ui_ccnotepad.h" line="1070"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="722"/>
        <location filename="ui_ccnotepad.h" line="1071"/>
        <source>BugFix</source>
        <translation type="unfinished">问题反馈</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1431"/>
        <location filename="ui_ccnotepad.h" line="1163"/>
        <source>Donate</source>
        <translation type="unfinished">捐赠作者</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1442"/>
        <location filename="ui_ccnotepad.h" line="1164"/>
        <source>Default</source>
        <translation type="unfinished">默认</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1450"/>
        <location filename="ui_ccnotepad.h" line="1165"/>
        <source>LightBlue</source>
        <translation type="unfinished">亮蓝</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1458"/>
        <location filename="ui_ccnotepad.h" line="1166"/>
        <source>ThinBlue</source>
        <translation type="unfinished">淡蓝</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1466"/>
        <location filename="ui_ccnotepad.h" line="1167"/>
        <source>RiceYellow</source>
        <translation type="unfinished">纸黄</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1474"/>
        <location filename="ui_ccnotepad.h" line="1168"/>
        <source>Yellow</source>
        <translation type="unfinished">黄色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1482"/>
        <location filename="ui_ccnotepad.h" line="1169"/>
        <source>Silver</source>
        <translation type="unfinished">银色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1490"/>
        <location filename="ui_ccnotepad.h" line="1170"/>
        <source>LavenderBlush</source>
        <translation type="unfinished">淡紫红</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1498"/>
        <location filename="ui_ccnotepad.h" line="1171"/>
        <source>MistyRose</source>
        <translation type="unfinished">浅玫瑰色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1506"/>
        <location filename="ui_ccnotepad.h" line="1172"/>
        <source>English</source>
        <translation type="unfinished">英文</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1514"/>
        <location filename="ui_ccnotepad.h" line="1173"/>
        <source>Chinese</source>
        <translation type="unfinished">中文</translation>
    </message>
    <message>
        <source>donate</source>
        <translation type="obsolete">捐赠软件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1403"/>
        <location filename="ui_ccnotepad.h" line="1158"/>
        <source>TXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1426"/>
        <location filename="ui_ccnotepad.h" line="1162"/>
        <source>Search Result</source>
        <translation type="unfinished">查找结果</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1408"/>
        <location filename="cceditor/ccnotepad.ui" line="1413"/>
        <location filename="ui_ccnotepad.h" line="1159"/>
        <location filename="ui_ccnotepad.h" line="1160"/>
        <source>test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1421"/>
        <location filename="ui_ccnotepad.h" line="1161"/>
        <source>go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="727"/>
        <location filename="ui_ccnotepad.h" line="1072"/>
        <source>File compare</source>
        <translation type="unfinished">文件对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="20"/>
        <location filename="ui_ccnotepad.h" line="988"/>
        <source>notepad--</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="117"/>
        <location filename="ui_ccnotepad.h" line="1186"/>
        <source>Blank CharOperate</source>
        <translation type="unfinished">空白字符操作</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="412"/>
        <location filename="ui_ccnotepad.h" line="1214"/>
        <source>Feedback</source>
        <translation type="unfinished">反馈问题</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="732"/>
        <location filename="ui_ccnotepad.h" line="1073"/>
        <source>Dir compare</source>
        <translation type="unfinished">目录对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="745"/>
        <location filename="ui_ccnotepad.h" line="1075"/>
        <source>XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="753"/>
        <location filename="ui_ccnotepad.h" line="1076"/>
        <source>YAML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="761"/>
        <location filename="ui_ccnotepad.h" line="1077"/>
        <source>Php</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="777"/>
        <location filename="ui_ccnotepad.h" line="1079"/>
        <source>C++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="785"/>
        <location filename="ui_ccnotepad.h" line="1080"/>
        <source>C#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="793"/>
        <location filename="ui_ccnotepad.h" line="1081"/>
        <source>Objective C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="801"/>
        <location filename="ui_ccnotepad.h" line="1082"/>
        <source>Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="809"/>
        <location filename="ui_ccnotepad.h" line="1083"/>
        <source>RC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="817"/>
        <location filename="ui_ccnotepad.h" line="1084"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="825"/>
        <location filename="ui_ccnotepad.h" line="1085"/>
        <source>Makefile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="833"/>
        <location filename="ui_ccnotepad.h" line="1086"/>
        <source>Pascal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="841"/>
        <location filename="ui_ccnotepad.h" line="1087"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="849"/>
        <location filename="ui_ccnotepad.h" line="1088"/>
        <source>ini</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="857"/>
        <location filename="ui_ccnotepad.h" line="1089"/>
        <source>Nfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="865"/>
        <location filename="ui_ccnotepad.h" line="1090"/>
        <source>Asp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="873"/>
        <location filename="ui_ccnotepad.h" line="1091"/>
        <source>Sql</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="881"/>
        <location filename="ui_ccnotepad.h" line="1092"/>
        <source>Virsual Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="889"/>
        <location filename="ui_ccnotepad.h" line="1093"/>
        <source>JavaScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="897"/>
        <location filename="ui_ccnotepad.h" line="1094"/>
        <source>Css</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="905"/>
        <location filename="ui_ccnotepad.h" line="1095"/>
        <source>Perl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="913"/>
        <location filename="ui_ccnotepad.h" line="1096"/>
        <source>Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="921"/>
        <location filename="ui_ccnotepad.h" line="1097"/>
        <source>Lua</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="929"/>
        <location filename="ui_ccnotepad.h" line="1098"/>
        <source>Tex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="937"/>
        <location filename="ui_ccnotepad.h" line="1099"/>
        <source>Fortran</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="945"/>
        <location filename="ui_ccnotepad.h" line="1100"/>
        <source>Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="953"/>
        <location filename="ui_ccnotepad.h" line="1101"/>
        <source>ActionScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="961"/>
        <location filename="ui_ccnotepad.h" line="1102"/>
        <source>NSIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="969"/>
        <location filename="ui_ccnotepad.h" line="1103"/>
        <source>Tcl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="977"/>
        <location filename="ui_ccnotepad.h" line="1104"/>
        <source>Lisp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="985"/>
        <location filename="ui_ccnotepad.h" line="1105"/>
        <source>Scheme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="993"/>
        <location filename="ui_ccnotepad.h" line="1106"/>
        <source>Assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1006"/>
        <location filename="ui_ccnotepad.h" line="1108"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1014"/>
        <location filename="ui_ccnotepad.h" line="1109"/>
        <source>Properties file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1022"/>
        <location filename="ui_ccnotepad.h" line="1110"/>
        <source>PostScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1030"/>
        <location filename="ui_ccnotepad.h" line="1111"/>
        <source>Ruby</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1038"/>
        <location filename="ui_ccnotepad.h" line="1112"/>
        <source>Smalltalk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1046"/>
        <location filename="ui_ccnotepad.h" line="1113"/>
        <source>VHDL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1054"/>
        <location filename="ui_ccnotepad.h" line="1114"/>
        <source>AutoIt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1062"/>
        <location filename="ui_ccnotepad.h" line="1115"/>
        <source>CMake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1070"/>
        <location filename="ui_ccnotepad.h" line="1116"/>
        <source>PowerShell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1078"/>
        <location filename="ui_ccnotepad.h" line="1117"/>
        <source>Jsp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1086"/>
        <location filename="ui_ccnotepad.h" line="1118"/>
        <source>CoffeeScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1094"/>
        <location filename="ui_ccnotepad.h" line="1119"/>
        <source>BaanC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1102"/>
        <location filename="ui_ccnotepad.h" line="1120"/>
        <source>S-Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1110"/>
        <location filename="ui_ccnotepad.h" line="1121"/>
        <source>TypeScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1118"/>
        <location filename="ui_ccnotepad.h" line="1122"/>
        <source>Visual Prolog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1126"/>
        <location filename="ui_ccnotepad.h" line="1123"/>
        <source>Txt2tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1134"/>
        <location filename="ui_ccnotepad.h" line="1124"/>
        <source>Rust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1142"/>
        <location filename="ui_ccnotepad.h" line="1125"/>
        <source>Registry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1150"/>
        <location filename="ui_ccnotepad.h" line="1126"/>
        <source>REBOL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1158"/>
        <location filename="ui_ccnotepad.h" line="1127"/>
        <source>OScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1166"/>
        <location filename="ui_ccnotepad.h" line="1128"/>
        <source>Nncrontab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1174"/>
        <location filename="ui_ccnotepad.h" line="1129"/>
        <source>Nim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1182"/>
        <location filename="ui_ccnotepad.h" line="1130"/>
        <source>MMIXAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1190"/>
        <location filename="ui_ccnotepad.h" line="1131"/>
        <source>LaTex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1198"/>
        <location filename="ui_ccnotepad.h" line="1132"/>
        <source>Forth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1206"/>
        <location filename="ui_ccnotepad.h" line="1133"/>
        <source>ESCRIPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1214"/>
        <location filename="ui_ccnotepad.h" line="1134"/>
        <source>Erlang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1222"/>
        <location filename="ui_ccnotepad.h" line="1135"/>
        <source>Csound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1230"/>
        <location filename="ui_ccnotepad.h" line="1136"/>
        <source>FreeBasic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1238"/>
        <location filename="ui_ccnotepad.h" line="1137"/>
        <source>BlitzBasic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1246"/>
        <location filename="ui_ccnotepad.h" line="1138"/>
        <source>PureBasic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1254"/>
        <location filename="ui_ccnotepad.h" line="1139"/>
        <source>AviSynth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1262"/>
        <location filename="ui_ccnotepad.h" line="1140"/>
        <source>ASN.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1270"/>
        <location filename="ui_ccnotepad.h" line="1141"/>
        <source>Swift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1278"/>
        <location filename="ui_ccnotepad.h" line="1142"/>
        <source>Intel HEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1286"/>
        <location filename="ui_ccnotepad.h" line="1143"/>
        <source>Fortran77</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1294"/>
        <location filename="ui_ccnotepad.h" line="1144"/>
        <source>Edifact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1299"/>
        <location filename="cceditor/ccnotepad.ui" line="1307"/>
        <location filename="ui_ccnotepad.h" line="1145"/>
        <location filename="ui_ccnotepad.h" line="1146"/>
        <source>MarkDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1315"/>
        <location filename="ui_ccnotepad.h" line="1147"/>
        <source>Octave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1323"/>
        <location filename="ui_ccnotepad.h" line="1148"/>
        <source>Po</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1331"/>
        <location filename="ui_ccnotepad.h" line="1149"/>
        <source>Pov</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1339"/>
        <location filename="ui_ccnotepad.h" line="1150"/>
        <source>json</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1355"/>
        <location filename="ui_ccnotepad.h" line="1152"/>
        <source>AVS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1363"/>
        <location filename="ui_ccnotepad.h" line="1153"/>
        <source>Bash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1371"/>
        <location filename="ui_ccnotepad.h" line="1154"/>
        <source>IDL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1379"/>
        <location filename="ui_ccnotepad.h" line="1155"/>
        <source>Matlab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1387"/>
        <location filename="ui_ccnotepad.h" line="1156"/>
        <source>Spice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1395"/>
        <location filename="ui_ccnotepad.h" line="1157"/>
        <source>Verilog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1519"/>
        <location filename="ui_ccnotepad.h" line="1174"/>
        <source>Register</source>
        <translation type="unfinished">注册版本</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1524"/>
        <location filename="ui_ccnotepad.h" line="1175"/>
        <source>Language Format</source>
        <translation type="unfinished">编程语言格式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1529"/>
        <location filename="ui_ccnotepad.h" line="1176"/>
        <source>Open In Text</source>
        <translation type="unfinished">以文本模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1534"/>
        <location filename="ui_ccnotepad.h" line="1177"/>
        <source>Open In Bin</source>
        <translation type="unfinished">以二进制模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1539"/>
        <location filename="ui_ccnotepad.h" line="1178"/>
        <source>Remove Head Blank</source>
        <translation type="unfinished">去除行首空白</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1544"/>
        <location filename="ui_ccnotepad.h" line="1179"/>
        <source>Remove  End Blank</source>
        <translation type="unfinished">去除行尾空白</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1549"/>
        <location filename="ui_ccnotepad.h" line="1180"/>
        <source>Remove Head End Blank</source>
        <translation type="unfinished">去除行首尾空白</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1554"/>
        <location filename="ui_ccnotepad.h" line="1181"/>
        <source>Column Block Editing</source>
        <translation type="unfinished">列块编辑</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1562"/>
        <location filename="ui_ccnotepad.h" line="1182"/>
        <source>Wrap</source>
        <translation type="unfinished">自动换行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1001"/>
        <source>Ln:0	Col:0</source>
        <translation type="unfinished">行 0 列 0</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1024"/>
        <source>Quit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1224"/>
        <source>Edit with Notepad--</source>
        <oldsource>Edit with Notebook CC</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1289"/>
        <source>Close Current Document</source>
        <translation type="unfinished">关闭当前文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1290"/>
        <source>Close Non-Current documents</source>
        <translation type="unfinished">关闭所有非当前文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1291"/>
        <source>Close Left All</source>
        <translation type="unfinished">关闭左边所有文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1292"/>
        <source>Close Right All</source>
        <translation type="unfinished">关闭右边所有文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1296"/>
        <source>Current Document Sava as...</source>
        <translation type="unfinished">当前文件另存为</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1298"/>
        <source>Show File in Explorer...</source>
        <translation type="unfinished">定位到文件路径</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1297"/>
        <source>Open in New Window</source>
        <translation type="unfinished">在新窗口中打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="866"/>
        <source>Can&apos;t Get Admin Auth, Open File %1 failed</source>
        <translation type="unfinished">获取管理员权限失败，打开文件 %1 失败。修改系统文件请以管理员权限执行ndd程序。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1239"/>
        <source>Please run in admin auth</source>
        <translation type="unfinished">请在管理员权限下执行程序</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1295"/>
        <source>Rename Current Document </source>
        <translation type="unfinished">重命名当前文件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1301"/>
        <source>Reload With Text Mode</source>
        <translation type="unfinished">重新以文本模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1302"/>
        <source>Reload With Hex Mode</source>
        <translation type="unfinished">重新以二进制模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1305"/>
        <source>Select Left Cmp File</source>
        <translation type="unfinished">选择为左边对比文件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1306"/>
        <source>Select Right Cmp File</source>
        <translation type="unfinished">选择为右边对比文件</translation>
    </message>
    <message>
        <source>Big Text File ReadOnly</source>
        <translation type="obsolete">大文本文件只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1612"/>
        <source>New File</source>
        <translation type="unfinished">新建</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1619"/>
        <source>Open File</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1626"/>
        <location filename="cceditor/ccnotepad.cpp" line="3294"/>
        <source>Save File</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1633"/>
        <source>Save All File</source>
        <translation type="unfinished">保存所有</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1641"/>
        <source>Cycle Auto Save</source>
        <translation type="unfinished">周期自动保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1718"/>
        <source>Mark</source>
        <translation type="unfinished">标记</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1728"/>
        <source>word highlight(F8)</source>
        <translation type="unfinished">高亮单词（F8)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1737"/>
        <source>clear all highlight(F7)</source>
        <translation type="unfinished">取消所有高亮（F7)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1748"/>
        <source>Zoom In</source>
        <translation type="unfinished">放大</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1755"/>
        <source>Zoom Out</source>
        <translation type="unfinished">缩小</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1766"/>
        <source>Word Wrap</source>
        <translation type="unfinished">自动换行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1785"/>
        <source>Show Blank</source>
        <translation type="unfinished">显示空白字符</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1794"/>
        <source>Indent Guide</source>
        <translation type="unfinished">缩进参考线</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1803"/>
        <source>Pre Hex Page</source>
        <translation type="unfinished">上一页/位置</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1810"/>
        <source>Next Hex Page</source>
        <translation type="unfinished">下一页/位置</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1817"/>
        <source>Goto Hex Page</source>
        <translation type="unfinished">跳转到文件偏移地址</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1826"/>
        <source>File Compare</source>
        <translation type="unfinished">文件对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1833"/>
        <source>Dir Compare</source>
        <translation type="unfinished">目录对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1840"/>
        <source>Bin Compare</source>
        <translation type="unfinished">二进制对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1849"/>
        <source>transform encoding</source>
        <translation type="unfinished">转换编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1856"/>
        <source>batch rename file</source>
        <translation type="unfinished">批量重命名</translation>
    </message>
    <message>
        <source>The window background that has been opened will take effect after it is reopened.</source>
        <translation type="obsolete">已打开的窗口背景颜色，将在文件重新打开后才会生效！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2177"/>
        <source>GB18030(Simplified Chinese)</source>
        <translation type="unfinished">GB18030(简体中文)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2190"/>
        <source>Language: %1</source>
        <translation type="unfinished">语法：%1</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2211"/>
        <source>Reload</source>
        <translation type="unfinished">重加载</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2369"/>
        <source>Yes</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2369"/>
        <location filename="cceditor/ccnotepad.cpp" line="2709"/>
        <source>No</source>
        <translation type="unfinished">放弃修改</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2369"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2497"/>
        <location filename="cceditor/ccnotepad.cpp" line="5912"/>
        <source>Restore Last Temp File %1 Failed</source>
        <translation type="unfinished">恢复临时文件 %1 失败！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2709"/>
        <source>Recover File?</source>
        <translation type="unfinished">是否恢复文件？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2709"/>
        <source>File %1 abnormally closed last time , Restore it ?</source>
        <translation type="unfinished">文件 %1 上次异常退出并留下未保存存档，是否恢复文件存档？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2709"/>
        <source>Restore</source>
        <translation type="unfinished">恢复文件？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2755"/>
        <source>File %1 Open Failed</source>
        <translation type="unfinished">文件 %1 打开失败！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2816"/>
        <location filename="cceditor/ccnotepad.cpp" line="5989"/>
        <source>File %1 Open Finished [Text Mode]</source>
        <translation type="unfinished">文件 %1 打开成功 [文本模式]</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2853"/>
        <location filename="cceditor/ccnotepad.cpp" line="2934"/>
        <source>Current offset is %1 , load Contens Size is %2, File Total Size is %3</source>
        <translation type="unfinished">当前文件偏移 %1 ， 加载内容大小是 %2，文件总大小是 %3 （字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2988"/>
        <source>File %1 Open Finished [Hex ReayOnly Mode]</source>
        <translation type="unfinished">文件 %1 打开成功 [二进制只读模式]</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3044"/>
        <source>file %1 may be a hex file , try open with text file.</source>
        <translation type="unfinished">文件 %1 可能是二进制文件，尝试以文本格式打开。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3132"/>
        <source>Save File %1 failed. You may not have write privileges 
Please save as a new file!</source>
        <translation type="unfinished">保存文件 %1 失败！ 你可能没有文件写权限，请另存为一个新文件！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3653"/>
        <source>Cycle autosave on ...</source>
        <translation type="unfinished">周期性自动保存文件已开启...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3661"/>
        <source>Cycle autosave off ...</source>
        <translation type="unfinished">周期性自动保存文件已关闭...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3718"/>
        <source>The current document has been automatically saved</source>
        <translation type="unfinished">当前文件周期性自动保存完毕！</translation>
    </message>
    <message>
        <source>file %1 was closed !</source>
        <translation type="obsolete">文件 %1 已经关闭 ！</translation>
    </message>
    <message>
        <source>&quot;%1&quot;

 
This file has been modified by another program.
Do you want to reload it?</source>
        <translation type="obsolete">%1\n\n \n文件已在外部被其它程序修改。\n是否重新加载该文件?</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2228"/>
        <source>Ln: %1	Col: %2</source>
        <translation type="unfinished">行：%1 列：%2</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2369"/>
        <source>Save File?</source>
        <translation type="unfinished">保存文件？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2369"/>
        <source>if save file %1 ?</source>
        <translation type="unfinished">是否保存文件 %1 ？</translation>
    </message>
    <message>
        <source>Current offset is %1 , File Size is %2</source>
        <translation type="obsolete">当前文件偏移 %1 ， 文件大小是 %2 （字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="866"/>
        <location filename="cceditor/ccnotepad.cpp" line="3005"/>
        <location filename="cceditor/ccnotepad.cpp" line="3104"/>
        <location filename="cceditor/ccnotepad.cpp" line="3132"/>
        <location filename="cceditor/ccnotepad.cpp" line="3301"/>
        <location filename="cceditor/ccnotepad.cpp" line="3414"/>
        <location filename="cceditor/ccnotepad.cpp" line="3463"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3005"/>
        <source>file %1 not exist.</source>
        <translation type="unfinished">文件 %1 不存在</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3017"/>
        <location filename="cceditor/ccnotepad.cpp" line="5278"/>
        <source>file %1 already open at tab %2</source>
        <translation type="unfinished">文件 %1 已经在页面 %2 中打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3104"/>
        <location filename="cceditor/ccnotepad.cpp" line="3106"/>
        <source>Save File %1 failed. Can not write auth, Please save as new file</source>
        <translation type="unfinished">保存 %1 失败。当前文件没有写权限，请另存为一个新文件</translation>
    </message>
    <message>
        <source>Open File %1 failed</source>
        <translation type="obsolete">打开文件 %1 失败</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2583"/>
        <location filename="cceditor/ccnotepad.cpp" line="3269"/>
        <location filename="cceditor/ccnotepad.cpp" line="3439"/>
        <location filename="cceditor/ccnotepad.cpp" line="4650"/>
        <source>Only Text File Can Use it, Current Doc is a Hex File !</source>
        <translation type="unfinished">只有文本模式才能使用该功能，当前文件是二进制文件！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2211"/>
        <source>&quot;%1&quot; This file has been modified by another program. Do you want to reload it?</source>
        <translation type="unfinished">%1 该文件已在外部被其它程序修改，是否重新加载？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="842"/>
        <source>Run As Admin Failed to save the file. Please check the file permissions.</source>
        <translation type="unfinished">以管理员模式保存文件失败！请检查文件的权限。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1068"/>
        <source>If display exceptions,Please Install System Font Courier</source>
        <translation type="unfinished">如果界面字体不满意，还请安装windows系统字体 Courier</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1370"/>
        <source>The currently file %1 is already in text mode</source>
        <translation type="unfinished">当前文件 %1 已经是文本模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1386"/>
        <source>The currently file %1 is already in bin mode</source>
        <translation type="unfinished">当前文件 %1 已经是二进制模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2822"/>
        <location filename="cceditor/ccnotepad.cpp" line="5995"/>
        <source>File %1 Open Finished [Text ReadOnly Mode] (Note: display up to 50K bytes ...)</source>
        <translation type="unfinished">文件 %1 打开成功 [文本只读模式] （乱码：二进制文件强行以文本格式显示，最多显示50K字节的内容，后面忽略...）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3301"/>
        <location filename="cceditor/ccnotepad.cpp" line="3463"/>
        <source>file %1 already open at tab %2, please select other file name.</source>
        <translation type="unfinished">文件 %1 已经存在于页面 %2 中，请选择一个其它名称</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3375"/>
        <source>Rename File As ...</source>
        <translation type="unfinished">重命名...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3414"/>
        <source>file %1 reanme failed!</source>
        <translation type="unfinished">文件 %1 重命名失败！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3454"/>
        <location filename="cceditor/ccnotepad.cpp" line="3489"/>
        <source>Save File As ...</source>
        <translation type="unfinished">另存为文件 ...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3966"/>
        <source>Close ?</source>
        <translation type="unfinished">关闭?</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3966"/>
        <source>already has child window open, close all ?</source>
        <translation type="unfinished">目前还有子窗口处于打开状态，关闭所有窗口吗？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4448"/>
        <source>Find result</source>
        <translation type="unfinished">查找结果</translation>
    </message>
    <message>
        <source>file was closed !</source>
        <translation type="obsolete">文件已关闭</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4570"/>
        <location filename="cceditor/ccnotepad.cpp" line="4581"/>
        <source>Find result - %1 hit</source>
        <translation type="unfinished">查找结果 - %1 命中</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4682"/>
        <source>Convert end of line In progress, please wait ...</source>
        <translation type="unfinished">行尾转换中，请等待...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4687"/>
        <source>Convert end of line finish.</source>
        <translation type="unfinished">行尾转换完毕</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4711"/>
        <source>Go to line</source>
        <translation type="unfinished">跳转到行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4711"/>
        <source>Line Num:</source>
        <translation type="unfinished">行号</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5013"/>
        <source>no more pre pos</source>
        <translation type="unfinished">没有前一个位置了</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5050"/>
        <source>no more next pos</source>
        <translation type="unfinished">没有后一个位置了</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5068"/>
        <location filename="cceditor/ccnotepad.cpp" line="5085"/>
        <source>The Last Page ! Current offset is %1 , load Contens Size is %2, File Total Size is %3</source>
        <translation type="unfinished">最后一页！当前文件偏移是 %1 ，加载内容大小是 %2 ，文件总大小是 %3 (字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5120"/>
        <source>Only Hex File Can Use it, Current Doc not a Hex File !</source>
        <translation type="unfinished">只有二进制文件具备该功能。当前文件不是二进制文件！</translation>
    </message>
    <message>
        <source>The Last Page ! Current offset is %1 , File Size is %2</source>
        <translation type="obsolete">最后一页！当前文件偏移是 %1 ，文件大小是 %2 (字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4529"/>
        <source>file %1 was not exists !</source>
        <translation type="unfinished">文件 %1 不存在！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5136"/>
        <location filename="cceditor/ccnotepad.cpp" line="5161"/>
        <source>Error file offset addr , please check !</source>
        <translation type="unfinished">错误的文件偏移量地址，请检查！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5149"/>
        <location filename="cceditor/ccnotepad.cpp" line="5174"/>
        <source>File Size is %1, addr %2 is exceeds file size</source>
        <translation type="unfinished">文件大小是 %1，当前地址 %2 超过了文件大小。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5180"/>
        <source>Current Text Doc Can Not Use it !</source>
        <oldsource>Current Text Doc Canp Not Use it !</oldsource>
        <translation type="unfinished">当前是常规文本文档，不能使用该功能！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5189"/>
        <location filename="cceditor/ccnotepad.cpp" line="5193"/>
        <source>bugfix: https://github.com/cxasm/notepad-- 
china: https://gitee.com/cxasm/notepad--</source>
        <oldsource>bugfix: https://github.com/cxasm/notepad--</oldsource>
        <translation type="unfinished">bug反馈：https://github.com/cxasm/notepad--
国内：https://gitee.com/cxasm/notepad--</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5596"/>
        <location filename="cceditor/ccnotepad.cpp" line="5630"/>
        <source>notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5596"/>
        <location filename="cceditor/ccnotepad.cpp" line="5630"/>
        <source>file path not exist, remove recent record!</source>
        <translation type="unfinished">文件路径不存在，删除历史记录！</translation>
    </message>
</context>
<context>
    <name>CTipWin</name>
    <message>
        <location filename="ctipwin.ui" line="30"/>
        <location filename="ui_ctipwin.h" line="63"/>
        <source>Msg Tips</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CmpareMode</name>
    <message>
        <location filename="CmpareMode.cpp" line="188"/>
        <location filename="CmpareMode.cpp" line="193"/>
        <source>Error : Max Bin File Size is 10M ! Exceeding file size !</source>
        <translation type="unfinished">错误：二进制对比最大文件为10M ! 超过限制。</translation>
    </message>
    <message>
        <location filename="CmpareMode.cpp" line="1487"/>
        <source>File Start Pos Exceeding File Size !</source>
        <translation type="unfinished">文件开始偏移位置超过文件长度！</translation>
    </message>
</context>
<context>
    <name>ColumnEdit</name>
    <message>
        <location filename="columnedit.ui" line="20"/>
        <location filename="ui_columnedit.h" line="274"/>
        <source>ColumnEdit</source>
        <translation type="unfinished">列块编辑</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="44"/>
        <location filename="ui_columnedit.h" line="275"/>
        <source>Insert Text</source>
        <translation type="unfinished">插入文本</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="61"/>
        <location filename="ui_columnedit.h" line="276"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="68"/>
        <location filename="ui_columnedit.h" line="277"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="79"/>
        <location filename="ui_columnedit.h" line="278"/>
        <source>Insert Num</source>
        <translation type="unfinished">插入数字</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="99"/>
        <location filename="ui_columnedit.h" line="279"/>
        <source>Initial value:</source>
        <translation type="unfinished">初始值：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="106"/>
        <location filename="columnedit.ui" line="112"/>
        <location filename="ui_columnedit.h" line="280"/>
        <location filename="ui_columnedit.h" line="281"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="139"/>
        <location filename="ui_columnedit.h" line="282"/>
        <source>increment:</source>
        <translation type="unfinished">增量值：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="179"/>
        <location filename="ui_columnedit.h" line="283"/>
        <source>Repetitions:</source>
        <translation type="unfinished">重复次数：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="219"/>
        <location filename="ui_columnedit.h" line="284"/>
        <source>prefix string:</source>
        <translation type="unfinished">前缀字符串：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="248"/>
        <location filename="ui_columnedit.h" line="285"/>
        <source>Format</source>
        <translation type="unfinished">格式</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="256"/>
        <location filename="ui_columnedit.h" line="286"/>
        <source>Decimal </source>
        <translation type="unfinished">十进制</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="266"/>
        <location filename="ui_columnedit.h" line="287"/>
        <source>Hex</source>
        <translation type="unfinished">16进制</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="277"/>
        <location filename="ui_columnedit.h" line="288"/>
        <source>Octal </source>
        <translation type="unfinished">八进制</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="284"/>
        <location filename="ui_columnedit.h" line="289"/>
        <source>Binary</source>
        <translation type="unfinished">二进制</translation>
    </message>
</context>
<context>
    <name>CompareDirs</name>
    <message>
        <location filename="CompareDirs.ui" line="14"/>
        <location filename="ui_CompareDirs.h" line="218"/>
        <source>NetRegister Dirs</source>
        <translation type="unfinished">对比文件夹</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="84"/>
        <location filename="CompareDirs.ui" line="212"/>
        <location filename="ui_CompareDirs.h" line="220"/>
        <location filename="ui_CompareDirs.h" line="232"/>
        <source>Open Dir</source>
        <translatorcomment>打开文件夹</translatorcomment>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="98"/>
        <location filename="CompareDirs.ui" line="226"/>
        <location filename="ui_CompareDirs.h" line="224"/>
        <location filename="ui_CompareDirs.h" line="236"/>
        <source>Reload Dir</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="144"/>
        <location filename="CompareDirs.ui" line="266"/>
        <location filename="ui_CompareDirs.h" line="230"/>
        <location filename="ui_CompareDirs.h" line="242"/>
        <source>Name</source>
        <translation type="unfinished">文件名</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="149"/>
        <location filename="CompareDirs.ui" line="271"/>
        <location filename="ui_CompareDirs.h" line="229"/>
        <location filename="ui_CompareDirs.h" line="241"/>
        <source>FileSize</source>
        <translation type="unfinished">大小</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="154"/>
        <location filename="CompareDirs.ui" line="276"/>
        <location filename="ui_CompareDirs.h" line="228"/>
        <location filename="ui_CompareDirs.h" line="240"/>
        <source>Modify Time</source>
        <oldsource>Time</oldsource>
        <translation type="unfinished">时间</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="69"/>
        <source>rule</source>
        <translation type="unfinished">规则</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="78"/>
        <source>all</source>
        <translation type="unfinished">全部</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="86"/>
        <source>diff</source>
        <translation type="unfinished">不同</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="96"/>
        <source>clear</source>
        <translation type="unfinished">清空</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="104"/>
        <source>swap</source>
        <translation type="unfinished">交换</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="112"/>
        <source>reload</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="206"/>
        <source>Status: normal</source>
        <translation type="unfinished">状态：正常</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="399"/>
        <location filename="CompareDirs.cpp" line="422"/>
        <source>error: %1 not a dir !</source>
        <translation type="unfinished">错误：%1 不是一个目录！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="525"/>
        <source>now busy, please try later ...</source>
        <translation type="unfinished">当前忙，稍后再试...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="596"/>
        <location filename="CompareDirs.cpp" line="615"/>
        <source>Copy to right</source>
        <translation type="unfinished">拷贝到右边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="597"/>
        <location filename="CompareDirs.cpp" line="616"/>
        <source>Cover Diffent File To Right</source>
        <translation type="unfinished">覆盖不同文件到右边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="601"/>
        <location filename="CompareDirs.cpp" line="610"/>
        <source>Copy to left</source>
        <translation type="unfinished">拷贝到左边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="602"/>
        <location filename="CompareDirs.cpp" line="611"/>
        <source>Cover Diffent File To Left</source>
        <translation type="unfinished">覆盖不同文件到左边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="627"/>
        <source>Mark as equal</source>
        <translation type="unfinished">标记为相等</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="635"/>
        <source>Copy Unique File To Other Side</source>
        <translation type="unfinished">拷贝独有文件到对方</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="641"/>
        <source>Delete This File</source>
        <translation type="unfinished">删除该文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="645"/>
        <source>Delete Only in This Side</source>
        <translation type="unfinished">删除本侧独有文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="651"/>
        <source>Cope Path To Clipboard</source>
        <translation type="unfinished">拷贝文件路径到剪切板</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="671"/>
        <source>Find File By Name</source>
        <translation type="unfinished">查找文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="715"/>
        <source>%1 not exist, please check!</source>
        <translation type="unfinished">文件 %1 不存在，请检查！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="716"/>
        <location filename="CompareDirs.cpp" line="1069"/>
        <location filename="CompareDirs.cpp" line="1104"/>
        <location filename="CompareDirs.cpp" line="1139"/>
        <source>Notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="745"/>
        <source>del file %1 success!</source>
        <translation type="unfinished">删除文件 %1 成功！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="749"/>
        <source>del file %1 failed, maybe other place using !</source>
        <translation type="unfinished">删除文件 %1 失败，可能其它地方在使用中！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="870"/>
        <source>right Dirs No Find Prev!</source>
        <translation type="unfinished">右边目录没有找到前一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="871"/>
        <source>right Dirs No Find Next!</source>
        <translation type="unfinished">右边目录没有找到下一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="928"/>
        <source>Not Find</source>
        <translation type="unfinished">没有找到</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="871"/>
        <source>left Dirs No Find Next!</source>
        <translation type="unfinished">左边目录没有找到下一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="870"/>
        <source>left Dirs No Find Prev!</source>
        <translation type="unfinished">左边目录没有找到上一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1069"/>
        <source>Do you want to overwrite all files (excluding folders) to the other side?</source>
        <translation type="unfinished">您确定覆盖目录下所有不同文件到对方吗？（不递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1089"/>
        <source>cover file %1 please waiting</source>
        <translation type="unfinished">覆盖文件 %1 请等待</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1095"/>
        <location filename="CompareDirs.cpp" line="1129"/>
        <source>cover file finish, total cover %1 files</source>
        <translation type="unfinished">覆盖文件完成，一共覆盖 %1 个文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1104"/>
        <source>Do you want to copy unique files (excluding folders) to the other side?</source>
        <translation type="unfinished">您确定拷贝此目录下独有文件到对方？（不递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1139"/>
        <source>Do you want to delete all files (excluding folders) only in this side?</source>
        <translation type="unfinished">您确定删除目录下的独有文件吗？（不递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1165"/>
        <source>delete file finish, total del %1 files</source>
        <translation type="unfinished">删除文件完成，一共删除 %1 个文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1247"/>
        <location filename="CompareDirs.cpp" line="1331"/>
        <location filename="CompareDirs.cpp" line="1421"/>
        <source>%1 not exist, skip ...</source>
        <translation type="unfinished">文件 %1 不存在，跳过...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1256"/>
        <location filename="CompareDirs.cpp" line="1337"/>
        <location filename="CompareDirs.cpp" line="1428"/>
        <source>%1 is exist, if replace ?</source>
        <translation type="unfinished">%1 已经存在，是否替换文件？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1258"/>
        <location filename="CompareDirs.cpp" line="1339"/>
        <location filename="CompareDirs.cpp" line="1430"/>
        <source>Replace ?</source>
        <translation type="unfinished">是否替换？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1313"/>
        <location filename="CompareDirs.cpp" line="1413"/>
        <location filename="CompareDirs.cpp" line="1485"/>
        <source>copy file %1 failed, please check file auth !</source>
        <translation type="unfinished">拷贝文件 %1 失败，请检查文件权限！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1501"/>
        <location filename="CompareDirs.cpp" line="1512"/>
        <source>current file: %1</source>
        <oldsource>current file %1</oldsource>
        <translation type="unfinished">当前文件：%1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2004"/>
        <source>current exec rule mode is quick mode, please wait ...</source>
        <translation type="unfinished">当前执行的对比模式是快速模式，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2008"/>
        <source>current exec rule mode is deep slow mode, please wait ...</source>
        <translation type="unfinished">当前执行的对比模式是深入文本慢速模式，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2042"/>
        <source>There are still %1 files haven&apos;t returned comparison results</source>
        <translation type="unfinished">还有 %1 个文件正在对比中</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2046"/>
        <source>file is %1 in comparing !</source>
        <translation type="unfinished">文件 %1 对比进行中！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2789"/>
        <source>load dir file tree in progress
, please wait ...</source>
        <translation type="unfinished">正在加载文件树目录，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2818"/>
        <location filename="CompareDirs.cpp" line="2826"/>
        <source>skip dir %1</source>
        <translation type="unfinished">跳过目录 %1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2849"/>
        <source>load %1 dir %2</source>
        <translation type="unfinished">加载第 %1 个目录 %2</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2886"/>
        <source>skip file ext %1</source>
        <translation type="unfinished">跳过文件类型 %1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2892"/>
        <source>skip file prefix %1</source>
        <translation type="unfinished">跳过文件前缀 %1</translation>
    </message>
    <message>
        <source>file [%1] not a text file, can&apos;t cmpare !!!</source>
        <translation type="obsolete">文件 [%1] 不是文本文件，不能进行比较！！！</translation>
    </message>
    <message>
        <source>file [%1] may be not a text file, cmp is dangerous!
 Forced comparison ?</source>
        <translation type="obsolete">文件 [%1] 可能不是文本文件，比较操作危险，强行比较吗？（不建议）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1871"/>
        <location filename="CompareDirs.cpp" line="1888"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1952"/>
        <source>init dir file tree in progress
total %1 file, please wait ...</source>
        <translation type="unfinished">初始化文件树目录中，一共 %1 文件，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1918"/>
        <location filename="CompareDirs.cpp" line="1941"/>
        <location filename="CompareDirs.cpp" line="2000"/>
        <source>Comparison in progress, please wait ...</source>
        <translation type="unfinished">对比文件进行中，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1560"/>
        <location filename="CompareDirs.cpp" line="1583"/>
        <location filename="CompareDirs.cpp" line="1743"/>
        <location filename="CompareDirs.cpp" line="1766"/>
        <source>file [%1] may be not a text file, cmp in hex mode?</source>
        <translation type="unfinished">文件 %1 可能不是文本格式，是否进行二进制格式对比？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2027"/>
        <location filename="CompareDirs.cpp" line="2028"/>
        <location filename="CompareDirs.cpp" line="2372"/>
        <location filename="CompareDirs.cpp" line="2923"/>
        <source>compare canceled ...</source>
        <translation type="unfinished">对比被取消</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2070"/>
        <source>compare not finished, user canceled ...</source>
        <translation type="unfinished">对比没有完成，用户取消</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2071"/>
        <location filename="CompareDirs.cpp" line="2155"/>
        <source>user canceled finished ...</source>
        <translation type="unfinished">用户取消完成</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2078"/>
        <location filename="CompareDirs.cpp" line="2159"/>
        <location filename="CompareDirs.cpp" line="2160"/>
        <source>compare file finish ...</source>
        <translation type="unfinished">文件对比完成</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2184"/>
        <source>load dir files, please wait ...</source>
        <translation type="unfinished">加载目录中，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2236"/>
        <source>load dir finish, total %1 files</source>
        <translation type="unfinished">加载目录完成，一共加载 %1 个文件</translation>
    </message>
</context>
<context>
    <name>CompareHexWin</name>
    <message>
        <location filename="comparehexwin.ui" line="20"/>
        <location filename="ui_comparehexwin.h" line="207"/>
        <source>NetRegister Bin File</source>
        <translation type="unfinished">二进制文件对比</translation>
    </message>
    <message>
        <location filename="comparehexwin.ui" line="81"/>
        <location filename="comparehexwin.ui" line="180"/>
        <location filename="ui_comparehexwin.h" line="209"/>
        <location filename="ui_comparehexwin.h" line="217"/>
        <source>Open File</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="comparehexwin.ui" line="104"/>
        <location filename="comparehexwin.ui" line="203"/>
        <location filename="ui_comparehexwin.h" line="213"/>
        <location filename="ui_comparehexwin.h" line="221"/>
        <source>Save File</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="49"/>
        <location filename="comparehexwin.cpp" line="50"/>
        <source>info</source>
        <translation type="unfinished">信息</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="58"/>
        <location filename="comparehexwin.cpp" line="59"/>
        <source>rule</source>
        <translation type="unfinished">规则</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="67"/>
        <source>clear</source>
        <translation type="unfinished">清空</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="78"/>
        <location filename="comparehexwin.cpp" line="79"/>
        <source>swap</source>
        <translation type="unfinished">交换</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="87"/>
        <location filename="comparehexwin.cpp" line="88"/>
        <source>reload</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="170"/>
        <source>Drag file support ...</source>
        <translation type="unfinished">支持文件拖动...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="446"/>
        <source>Comparison in progress, please wait ...</source>
        <translation type="unfinished">对比文件进行中，请等待...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="461"/>
        <source>Error : Max Bin File Size is 10M ! Exceeding file size !</source>
        <translation type="unfinished">错误：二进制对比最大文件为10M ! 超过限制。</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="490"/>
        <source>cmpare bin file in progress
please wait ...</source>
        <translation type="unfinished">二进制文件对比中，请等待...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="616"/>
        <source>now busy, please try later ...</source>
        <translation type="unfinished">当前忙，稍后再试...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="655"/>
        <location filename="comparehexwin.cpp" line="660"/>
        <source>Compare Result</source>
        <translation type="unfinished">对比结果</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="655"/>
        <location filename="comparehexwin.cpp" line="660"/>
        <source>Left size %1 byte, right size %2 byte 
Equal content size %3 
Left Equal ratio %4 Right Equal ratio %5</source>
        <translation type="unfinished">左文件大小 %1 右文件大小 %2
相等内容长度 %3 
左边相等率 %4 右边相等率%5</translation>
    </message>
</context>
<context>
    <name>CompareWin</name>
    <message>
        <location filename="comparewin.ui" line="20"/>
        <location filename="ui_comparewin.h" line="292"/>
        <source>NetRegister File</source>
        <translation type="unfinished">对比文件</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="100"/>
        <location filename="comparewin.ui" line="291"/>
        <location filename="ui_comparewin.h" line="294"/>
        <location filename="ui_comparewin.h" line="316"/>
        <source>Open File</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="120"/>
        <location filename="comparewin.ui" line="311"/>
        <location filename="ui_comparewin.h" line="298"/>
        <location filename="ui_comparewin.h" line="320"/>
        <source>Save File</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="151"/>
        <location filename="comparewin.cpp" line="3708"/>
        <location filename="comparewin.cpp" line="3720"/>
        <location filename="ui_comparewin.h" line="301"/>
        <source>left text code</source>
        <translation type="unfinished">左边编码</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="172"/>
        <location filename="comparewin.ui" line="360"/>
        <location filename="ui_comparewin.h" line="304"/>
        <location filename="ui_comparewin.h" line="326"/>
        <source>UTF16-LE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="177"/>
        <location filename="comparewin.ui" line="365"/>
        <location filename="ui_comparewin.h" line="305"/>
        <location filename="ui_comparewin.h" line="327"/>
        <source>UTF16-BG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="162"/>
        <location filename="comparewin.ui" line="350"/>
        <location filename="ui_comparewin.h" line="302"/>
        <location filename="ui_comparewin.h" line="324"/>
        <source>UTF-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="167"/>
        <location filename="comparewin.ui" line="355"/>
        <location filename="ui_comparewin.h" line="303"/>
        <location filename="ui_comparewin.h" line="325"/>
        <source>UTF-8 BOM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="182"/>
        <location filename="comparewin.ui" line="370"/>
        <location filename="ui_comparewin.h" line="306"/>
        <location filename="ui_comparewin.h" line="328"/>
        <source>GBK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="187"/>
        <location filename="comparewin.ui" line="375"/>
        <location filename="ui_comparewin.h" line="307"/>
        <location filename="ui_comparewin.h" line="329"/>
        <source>EUC-JP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="192"/>
        <location filename="comparewin.ui" line="380"/>
        <location filename="ui_comparewin.h" line="308"/>
        <location filename="ui_comparewin.h" line="330"/>
        <source>Shift-JIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="197"/>
        <location filename="comparewin.ui" line="385"/>
        <location filename="ui_comparewin.h" line="309"/>
        <location filename="ui_comparewin.h" line="331"/>
        <source>EUC-KR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="202"/>
        <location filename="comparewin.ui" line="390"/>
        <location filename="ui_comparewin.h" line="310"/>
        <location filename="ui_comparewin.h" line="332"/>
        <source>KOI8-R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="207"/>
        <location filename="comparewin.ui" line="395"/>
        <location filename="ui_comparewin.h" line="311"/>
        <location filename="ui_comparewin.h" line="333"/>
        <source>TSCII</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="212"/>
        <location filename="comparewin.ui" line="400"/>
        <location filename="ui_comparewin.h" line="312"/>
        <location filename="ui_comparewin.h" line="334"/>
        <source>TIS_620</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="217"/>
        <location filename="comparewin.ui" line="405"/>
        <location filename="ui_comparewin.h" line="313"/>
        <location filename="ui_comparewin.h" line="335"/>
        <source>UNKNOWN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="339"/>
        <location filename="comparewin.cpp" line="3707"/>
        <location filename="comparewin.cpp" line="3721"/>
        <location filename="ui_comparewin.h" line="323"/>
        <source>right text code</source>
        <translation type="unfinished">右边编码</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="224"/>
        <location filename="comparewin.cpp" line="225"/>
        <source>white</source>
        <translation type="unfinished">空白</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="232"/>
        <location filename="comparewin.cpp" line="233"/>
        <source>rule</source>
        <translation type="unfinished">规则</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="242"/>
        <location filename="comparewin.cpp" line="243"/>
        <source>break</source>
        <translation type="unfinished">打断</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="252"/>
        <source>pull</source>
        <translation type="unfinished">拉开</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="253"/>
        <source>pull open</source>
        <translation type="unfinished">拉开对比显示</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="265"/>
        <location filename="comparewin.cpp" line="266"/>
        <source>strict</source>
        <translation type="unfinished">严格</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="277"/>
        <location filename="comparewin.cpp" line="278"/>
        <source>ignore</source>
        <translation type="unfinished">忽略</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="288"/>
        <location filename="comparewin.cpp" line="289"/>
        <source>undo</source>
        <translation type="unfinished">撤销</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="299"/>
        <source>pre</source>
        <translation type="unfinished">上一个</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="300"/>
        <source>pre (F3)</source>
        <translation type="unfinished">上一个(F3)</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="309"/>
        <source>next</source>
        <translation type="unfinished">下一个</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="310"/>
        <source>next (F4)</source>
        <translation type="unfinished">下一个(F4)</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="322"/>
        <source>zoomin</source>
        <translation type="unfinished">放大</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="331"/>
        <source>zoomout</source>
        <translation type="unfinished">缩小</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="341"/>
        <source>clear</source>
        <translation type="unfinished">清空</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="349"/>
        <location filename="comparewin.cpp" line="350"/>
        <source>swap</source>
        <translation type="unfinished">交换</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="358"/>
        <source>reload</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="359"/>
        <source>reload (F5)</source>
        <translation type="unfinished">重载(F5)</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="521"/>
        <source>Drag file support ...</source>
        <translation type="unfinished">支持文件拖动...</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="1228"/>
        <location filename="comparewin.cpp" line="2757"/>
        <location filename="comparewin.cpp" line="3172"/>
        <location filename="comparewin.cpp" line="3184"/>
        <location filename="comparewin.cpp" line="4597"/>
        <location filename="comparewin.cpp" line="4926"/>
        <source>current has %1 differents</source>
        <translation type="unfinished">当前有 %1 处不同块</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3388"/>
        <source>save file finished !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3445"/>
        <location filename="comparewin.cpp" line="3490"/>
        <location filename="comparewin.cpp" line="3761"/>
        <location filename="comparewin.cpp" line="3794"/>
        <source>The left document has been modified.</source>
        <translation type="unfinished">左边文档已经被修改</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3449"/>
        <location filename="comparewin.cpp" line="3486"/>
        <location filename="comparewin.cpp" line="3765"/>
        <location filename="comparewin.cpp" line="3790"/>
        <source>The right document has been modified.</source>
        <translation type="unfinished">右边文档已经被修改</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3451"/>
        <location filename="comparewin.cpp" line="3492"/>
        <location filename="comparewin.cpp" line="3767"/>
        <location filename="comparewin.cpp" line="3796"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">是否保存修改？</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3454"/>
        <location filename="comparewin.cpp" line="3495"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3455"/>
        <location filename="comparewin.cpp" line="3496"/>
        <source>Discard</source>
        <translation type="unfinished">放弃修改</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3456"/>
        <location filename="comparewin.cpp" line="3497"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3557"/>
        <location filename="comparewin.cpp" line="3634"/>
        <source>no more unequal block!</source>
        <translation type="unfinished">没有更多不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3581"/>
        <source>the first one!</source>
        <translation type="unfinished">第一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3594"/>
        <location filename="comparewin.cpp" line="3671"/>
        <source>the %1 diff, total %2 diff</source>
        <translation type="unfinished">第 %1 处不同， 一共 %2 不同</translation>
    </message>
    <message>
        <source>the %1 diff,total %2 diff</source>
        <translation type="obsolete">第 %1 处不同，一共 %2 处不同。</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3601"/>
        <source>already the first one!</source>
        <translation type="unfinished">已经是第一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3658"/>
        <source>the last one!</source>
        <translation type="unfinished">最后一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3677"/>
        <source>already the last one!</source>
        <translation type="unfinished">已经是最后一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4067"/>
        <location filename="comparewin.cpp" line="4453"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4067"/>
        <location filename="comparewin.cpp" line="4453"/>
        <source>The current comparison has encountered an error.Quit temporarily.</source>
        <translation type="unfinished">当前对比发生未知错误，暂时退出。</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4406"/>
        <source>has %1 differents</source>
        <translation type="unfinished">有 %1 处不同</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4725"/>
        <source>Comparison in progress, please wait ...</source>
        <translation type="unfinished">对比文件进行中，请等待...</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4740"/>
        <location filename="comparewin.cpp" line="4748"/>
        <source>file [%1] maybe not a text file, forse cmpare?(dangerous, may be core)</source>
        <translation type="unfinished">文件 [%1] 可能不是文本,强行使用文本对比?(危险操作)</translation>
    </message>
    <message>
        <source>file [%1] not a text file, can&apos;t cmpare !!!</source>
        <translation type="obsolete">文件 [%1] 不是文本文件，不能进行比较！！！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4885"/>
        <source>no more undo operator!</source>
        <translation type="unfinished">没有更多撤销！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="5377"/>
        <source>now busy, please try later ...</source>
        <translation type="unfinished">当前忙，稍后再试...</translation>
    </message>
</context>
<context>
    <name>CompareWorker</name>
    <message>
        <location filename="compareworker.ui" line="14"/>
        <location filename="ui_compareworker.h" line="39"/>
        <source>CompareWorker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DirCmpExtWin</name>
    <message>
        <location filename="dircmpextwin.ui" line="14"/>
        <location filename="ui_dircmpextwin.h" line="183"/>
        <source>DirCmpExtWin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="20"/>
        <location filename="ui_dircmpextwin.h" line="184"/>
        <source>Cmp Mode</source>
        <translation type="unfinished">对比模式</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="36"/>
        <location filename="ui_dircmpextwin.h" line="186"/>
        <source>Compare file times and sizes (Fast mode)</source>
        <translation type="unfinished">对比文件大小和时间（快速模式）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="26"/>
        <location filename="ui_dircmpextwin.h" line="185"/>
        <source>Compare files in depth(slow mode,Accurate results)</source>
        <oldsource>Compare files in depth(slow mode)</oldsource>
        <translation type="unfinished">深度对比文件文本模式（慢速模式，结果更精确）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="49"/>
        <location filename="ui_dircmpextwin.h" line="187"/>
        <source>Dir Cmp Options</source>
        <translation type="unfinished">文件夹对比选项</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="72"/>
        <location filename="ui_dircmpextwin.h" line="190"/>
        <source>Compare Hide Dirs</source>
        <translation type="unfinished">对比隐藏目录</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="62"/>
        <location filename="ui_dircmpextwin.h" line="189"/>
        <source>Compare All Files</source>
        <translation type="unfinished">对比所有文件</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="55"/>
        <location filename="ui_dircmpextwin.h" line="188"/>
        <source>Compare Support Ext Files</source>
        <translation type="unfinished">对比已知后缀的文件</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="82"/>
        <location filename="ui_dircmpextwin.h" line="191"/>
        <source>Skip Dirs</source>
        <translation type="unfinished">跳过目录</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="100"/>
        <location filename="ui_dircmpextwin.h" line="192"/>
        <source>Skip Load these Dirs(Separated by:)</source>
        <translation type="unfinished">对比时跳过如下目录（以:号分割）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="107"/>
        <location filename="ui_dircmpextwin.h" line="193"/>
        <source>.svn:.vs:debug:Debug:release:Release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="120"/>
        <location filename="ui_dircmpextwin.h" line="194"/>
        <source>Skip File Exts</source>
        <translation type="unfinished">跳过文件类型</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="138"/>
        <location filename="ui_dircmpextwin.h" line="195"/>
        <source>Skip Cmpare these File Exts(Separated by:)</source>
        <translation type="unfinished">对比时跳过以下文件类型（以:号分割）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="145"/>
        <location filename="ui_dircmpextwin.h" line="196"/>
        <source>.sln:.vcxproj</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="158"/>
        <location filename="ui_dircmpextwin.h" line="197"/>
        <source>Skip FileName Prefix</source>
        <translation type="unfinished">跳过文件前缀</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="176"/>
        <location filename="ui_dircmpextwin.h" line="198"/>
        <source>Skip Cmpare these FileName Prefix(Separated by:)</source>
        <translation type="unfinished">对比时跳过以下文件名前缀（以:号分割）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="183"/>
        <location filename="ui_dircmpextwin.h" line="199"/>
        <source>ui_</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DirFindFile</name>
    <message>
        <location filename="dirfindfile.ui" line="26"/>
        <location filename="ui_dirfindfile.h" line="144"/>
        <source>DirFindFile</source>
        <translation type="unfinished">查找文件</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="36"/>
        <location filename="ui_dirfindfile.h" line="145"/>
        <source>Find Options</source>
        <translation type="unfinished">查找选项</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="44"/>
        <location filename="ui_dirfindfile.h" line="146"/>
        <source>Find File Name</source>
        <translation type="unfinished">查找文件名</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="58"/>
        <location filename="ui_dirfindfile.h" line="147"/>
        <source>find in left</source>
        <translation type="unfinished">在左边查找</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="68"/>
        <location filename="ui_dirfindfile.h" line="148"/>
        <source>find in right</source>
        <translation type="unfinished">在右边查找</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="75"/>
        <location filename="ui_dirfindfile.h" line="149"/>
        <source>case sensitive</source>
        <translation type="unfinished">区分大小写</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="92"/>
        <location filename="ui_dirfindfile.h" line="150"/>
        <source>Find Prev</source>
        <translation type="unfinished">查找前一个</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="99"/>
        <location filename="ui_dirfindfile.h" line="151"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="106"/>
        <location filename="ui_dirfindfile.h" line="152"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>DocTypeListView</name>
    <message>
        <location filename="doctypelistview.ui" line="14"/>
        <location filename="ui_doctypelistview.h" line="135"/>
        <source>DocTypeListView</source>
        <translation type="unfinished">文件关联列表</translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="22"/>
        <location filename="ui_doctypelistview.h" line="136"/>
        <source>Support file </source>
        <translation type="unfinished">支持文件类型</translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="71"/>
        <location filename="ui_doctypelistview.h" line="137"/>
        <source>-&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="78"/>
        <location filename="ui_doctypelistview.h" line="138"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="100"/>
        <location filename="ui_doctypelistview.h" line="139"/>
        <source>Custom extension</source>
        <translation type="unfinished">用户扩展类型</translation>
    </message>
    <message>
        <source>support file </source>
        <translation type="obsolete">支持文件后缀</translation>
    </message>
    <message>
        <location filename="doctypelistview.cpp" line="243"/>
        <source>input file ext()</source>
        <translation type="unfinished">输入文件后缀</translation>
    </message>
    <message>
        <location filename="doctypelistview.cpp" line="243"/>
        <source>ext (Split With :)</source>
        <translation type="unfinished">后缀（用:号分割开）</translation>
    </message>
</context>
<context>
    <name>EncodeConvert</name>
    <message>
        <location filename="encodeconvert.ui" line="14"/>
        <location filename="ui_encodeconvert.h" line="190"/>
        <source>EncodeConvert</source>
        <translation type="unfinished">文本编码转换</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="40"/>
        <location filename="ui_encodeconvert.h" line="196"/>
        <source>filePath</source>
        <translation type="unfinished">文件路径</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="45"/>
        <location filename="ui_encodeconvert.h" line="195"/>
        <source>file size</source>
        <translation type="unfinished">文件大小</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="50"/>
        <location filename="ui_encodeconvert.h" line="194"/>
        <source>file code</source>
        <translation type="unfinished">文件编码</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="55"/>
        <location filename="ui_encodeconvert.h" line="193"/>
        <source>convert code</source>
        <translation type="unfinished">转换为编码</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="60"/>
        <location filename="ui_encodeconvert.h" line="192"/>
        <source>convert result</source>
        <translation type="unfinished">转换结果</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="75"/>
        <location filename="ui_encodeconvert.h" line="197"/>
        <source>convert options</source>
        <translation type="unfinished">转换选项</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="83"/>
        <location filename="ui_encodeconvert.h" line="198"/>
        <source>convert to code</source>
        <translation type="unfinished">转换为编码</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="97"/>
        <location filename="ui_encodeconvert.h" line="199"/>
        <source>UTF8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="102"/>
        <location filename="ui_encodeconvert.h" line="200"/>
        <source>UTF8 BOM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="117"/>
        <location filename="ui_encodeconvert.h" line="203"/>
        <source>GBK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="107"/>
        <location filename="ui_encodeconvert.h" line="201"/>
        <source>UTF16-LE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="112"/>
        <location filename="ui_encodeconvert.h" line="202"/>
        <source>UTF16-BE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="129"/>
        <location filename="ui_encodeconvert.h" line="205"/>
        <source>deal file ext</source>
        <translation type="unfinished">处理文件类型</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="143"/>
        <location filename="ui_encodeconvert.h" line="206"/>
        <source>all support file ext</source>
        <translation type="unfinished">所有支持的文件类型</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="151"/>
        <location filename="ui_encodeconvert.h" line="209"/>
        <source>user defined</source>
        <translation type="unfinished">自定义</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="154"/>
        <location filename="ui_encodeconvert.h" line="211"/>
        <source>defined</source>
        <translation type="unfinished">自定义</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="181"/>
        <location filename="ui_encodeconvert.h" line="212"/>
        <source>select dir</source>
        <translation type="unfinished">选择目录</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="188"/>
        <location filename="ui_encodeconvert.h" line="213"/>
        <source>start</source>
        <translation type="unfinished">开始转换</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="198"/>
        <location filename="ui_encodeconvert.h" line="214"/>
        <source>close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="69"/>
        <source>&amp;Show File in Explorer...</source>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="99"/>
        <source>input file ext()</source>
        <translation type="unfinished">输入文件后缀</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="99"/>
        <source>ext (Split With :)</source>
        <translation type="unfinished">后缀（用:号分割开）</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="130"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="455"/>
        <source>start scan file text code, please wait...</source>
        <translation type="unfinished">开始扫描文件编码，请等待...</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="475"/>
        <location filename="encodeconvert.cpp" line="581"/>
        <source>ignore</source>
        <translation type="unfinished">忽略</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="489"/>
        <source>please wait, total file %1,cur scan index %2, scan finish %3%</source>
        <translation type="unfinished">请等待，一共 %1个文件，当前扫描第 %2 个，扫描完成率 %3%</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="493"/>
        <source>scan finished, total file %1</source>
        <translation type="unfinished">扫描完成，一共%1个文件</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="576"/>
        <source>already %1 ignore</source>
        <translation type="unfinished">已经是 %1编码,忽略</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="595"/>
        <source>total file %1,cur deal index %2,finish %3%</source>
        <translation type="unfinished">一共 %1 个文件，当前处理第 %2 个，完成率%3%</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="600"/>
        <source>total file %1,cur deal index %2,finish 100%</source>
        <translation type="unfinished">一共 %1 个 文件，当前处理第 %2 个，完成率100%</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="601"/>
        <source>convert finished !</source>
        <translation type="unfinished">转换完成！</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="622"/>
        <source>convert finish</source>
        <translation type="unfinished">转换完成</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="626"/>
        <source>convert fail</source>
        <translation type="unfinished">转换失败</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="627"/>
        <source>file %1 convert failed,pleas check...</source>
        <translation type="unfinished">文件 %1 转换编码失败，请检查...</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="698"/>
        <source>please drop a file dir ...</source>
        <translation type="unfinished">请拖入一个文件夹...</translation>
    </message>
</context>
<context>
    <name>FileCmpRuleWin</name>
    <message>
        <location filename="filecmprulewin.ui" line="20"/>
        <location filename="ui_filecmprulewin.h" line="160"/>
        <source>FileCmpRuleWin</source>
        <translation type="unfinished">文件对比规则</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="30"/>
        <location filename="ui_filecmprulewin.h" line="161"/>
        <source>Compare Options</source>
        <translation type="unfinished">对比选项</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="36"/>
        <location filename="ui_filecmprulewin.h" line="162"/>
        <source>Ignore whitespace characters before line</source>
        <oldsource>Ignore whitespace characters</oldsource>
        <translation type="unfinished">忽略行前行尾空白字符</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="46"/>
        <location filename="ui_filecmprulewin.h" line="163"/>
        <source>Ignore whitespace characters At back of the line(such as python)</source>
        <translation type="unfinished">只忽略行尾的空白字符（比如python等语言）</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="53"/>
        <location filename="ui_filecmprulewin.h" line="164"/>
        <source>Ignore all whitespace characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="63"/>
        <location filename="ui_filecmprulewin.h" line="165"/>
        <source>Match Options</source>
        <translation type="unfinished">匹配选项</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="69"/>
        <location filename="ui_filecmprulewin.h" line="166"/>
        <source>Blank lines participate in matching</source>
        <translation type="unfinished">空行参与匹配</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="81"/>
        <location filename="ui_filecmprulewin.h" line="167"/>
        <source>Identify matching rates for rows that are equal</source>
        <translation type="unfinished">认定为匹配行的相似率</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="89"/>
        <location filename="ui_filecmprulewin.h" line="168"/>
        <source>Match &gt;= 50%</source>
        <translation type="unfinished">匹配度 &gt;= 50%</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="94"/>
        <location filename="ui_filecmprulewin.h" line="169"/>
        <source>Match &gt;= 70%</source>
        <translation type="unfinished">匹配度 &gt;= 70%</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="99"/>
        <location filename="ui_filecmprulewin.h" line="170"/>
        <source>Match &gt;= 90%</source>
        <translation type="unfinished">匹配度 &gt;= 90%</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="140"/>
        <location filename="ui_filecmprulewin.h" line="172"/>
        <source>Apply</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="147"/>
        <location filename="ui_filecmprulewin.h" line="173"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>FileManager</name>
    <message>
        <location filename="cceditor/filemanager.cpp" line="177"/>
        <location filename="cceditor/filemanager.cpp" line="216"/>
        <location filename="cceditor/filemanager.cpp" line="228"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="177"/>
        <source>Open File %1 failed Can not read auth</source>
        <translation type="unfinished">打开文件 %1 失败。没有读文件的权限。</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="216"/>
        <source>Open File %1 failed</source>
        <translation type="unfinished">打开文件 %1 失败。</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="228"/>
        <source>File is too big to be opened by Notepad--</source>
        <translation type="unfinished">文件太大，不能使用Notepad--打开！</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="244"/>
        <source>The file %1 is likely to be binary. Do you want to open it in binary?</source>
        <translation type="unfinished">文件 %1 可能是二进制格式，你想以二进制（只读）格式打开文件吗？</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="244"/>
        <source>Open with Text or Hex?</source>
        <translation type="unfinished">二进制或文本打开？</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="244"/>
        <source>Hex Open</source>
        <translation type="unfinished">以二进制打开</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="244"/>
        <source>Text Open</source>
        <translation type="unfinished">以文本打开</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="244"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <source>File is too big to be opened by CC Notepad</source>
        <translation type="obsolete">文件太大不能使用Notepad打开！</translation>
    </message>
</context>
<context>
    <name>FindCmpWin</name>
    <message>
        <location filename="findcmpwin.ui" line="20"/>
        <location filename="ui_findcmpwin.h" line="224"/>
        <source>Find text window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="50"/>
        <location filename="ui_findcmpwin.h" line="239"/>
        <source>find</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="63"/>
        <location filename="ui_findcmpwin.h" line="225"/>
        <source>Find what :</source>
        <translation type="unfinished">查找目标：</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="88"/>
        <location filename="ui_findcmpwin.h" line="226"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="94"/>
        <location filename="ui_findcmpwin.h" line="227"/>
        <source>Backward direction</source>
        <translation type="unfinished">反向查找</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="101"/>
        <location filename="ui_findcmpwin.h" line="228"/>
        <source>Match whole word only</source>
        <translation type="unfinished">全词匹配</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="108"/>
        <location filename="ui_findcmpwin.h" line="229"/>
        <source>Match case</source>
        <translation type="unfinished">匹配大小写</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="115"/>
        <location filename="ui_findcmpwin.h" line="230"/>
        <source>Wrap around</source>
        <translation type="unfinished">循环查找</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="128"/>
        <location filename="ui_findcmpwin.h" line="231"/>
        <source>Search Mode</source>
        <translation type="unfinished">查找模式</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="136"/>
        <location filename="ui_findcmpwin.h" line="232"/>
        <source>Regular expression</source>
        <translation type="unfinished">正则表达式</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="145"/>
        <location filename="ui_findcmpwin.h" line="233"/>
        <source>Normal</source>
        <translation type="unfinished">普通</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="162"/>
        <location filename="ui_findcmpwin.h" line="234"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="175"/>
        <location filename="ui_findcmpwin.h" line="235"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="182"/>
        <location filename="ui_findcmpwin.h" line="236"/>
        <source>Diretion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="188"/>
        <location filename="ui_findcmpwin.h" line="237"/>
        <source>Search In Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="198"/>
        <location filename="ui_findcmpwin.h" line="238"/>
        <source>Search In Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.cpp" line="168"/>
        <source>what find is null !</source>
        <translation type="unfinished">查找字段为空</translation>
    </message>
    <message>
        <location filename="findcmpwin.cpp" line="186"/>
        <source>cant&apos;t find text &apos;%1&apos;</source>
        <translation type="unfinished">找不到字段 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findcmpwin.cpp" line="202"/>
        <source>no more find text &apos;%1&apos;</source>
        <translation type="unfinished">没有更多的字段 &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>FindResultWin</name>
    <message>
        <location filename="findresultwin.ui" line="20"/>
        <location filename="ui_findresultwin.h" line="63"/>
        <source>FindResultWin</source>
        <translation type="unfinished">查找结果</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="obsolete">清空</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="61"/>
        <source>clear this find result</source>
        <translation type="unfinished">清除当前结果</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="62"/>
        <source>clear all find result</source>
        <translation type="unfinished">清除所有结果</translation>
    </message>
    <message>
        <source>copy item content</source>
        <translation type="obsolete">拷贝到剪切板</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="70"/>
        <source>copy select item (Ctrl Muli)</source>
        <translation type="unfinished">复制选中项（按ctrl多选）</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="71"/>
        <source>copy select Line (Ctrl Muli)</source>
        <translation type="unfinished">复制选中行（按ctrl多选）</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="65"/>
        <source>select section</source>
        <translation type="unfinished">选中所在节</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="66"/>
        <source>select all item</source>
        <translation type="unfinished">全部选择</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="209"/>
        <location filename="findresultwin.cpp" line="255"/>
        <source>%1 rows selected !</source>
        <translation type="unfinished">%1 行被选中！</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="287"/>
        <source>%1 items have been copied to the clipboard !</source>
        <translation type="unfinished">%1 项已经被复制到剪切板</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="337"/>
        <source>%1 lines have been copied to the clipboard !</source>
        <translation type="unfinished">%1 行已经被复制到剪切板</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="397"/>
        <source>&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;Search &quot;%1&quot; (%2 hits)&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;查找 &quot;%1&quot; (%2 命中)&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="418"/>
        <location filename="findresultwin.cpp" line="499"/>
        <source>&lt;font style=&apos;font-weight:bold;color:#309730&apos;&gt;%1 (%2 hits)&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font style=&apos;font-weight:bold;color:#309730&apos;&gt;%1 (%2 命中)&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="452"/>
        <source>Line &lt;font style=&apos;color:#ff8040&apos;&gt;%1&lt;/font&gt; : %2</source>
        <translation type="unfinished">行 &lt;font style=&apos;color:#ff8040&apos;&gt;%1&lt;/font&gt; : %2</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="473"/>
        <source>&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;Search &quot;%1&quot; (%2 hits in %3 files)&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;查找 &quot;%1&quot; (%2 命中在 %3 个文件）&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; (%2 hits)</source>
        <translation type="obsolete">查找 &quot;%1&quot; (%2 命中）</translation>
    </message>
    <message>
        <source>%1 (%2 hits)</source>
        <translation type="obsolete">%1 (%2 命中)</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; (%2 hits in %3 files)</source>
        <translation type="obsolete">查找 &quot;%1&quot; (%2 命中在 %3 个文件）</translation>
    </message>
</context>
<context>
    <name>FindWin</name>
    <message>
        <location filename="findwin.ui" line="20"/>
        <location filename="ui_findwin.h" line="916"/>
        <source>MainWindow</source>
        <translation type="unfinished">查找与替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="53"/>
        <location filename="ui_findwin.h" line="933"/>
        <source>find</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="63"/>
        <location filename="findwin.ui" line="313"/>
        <location filename="findwin.ui" line="626"/>
        <location filename="ui_findwin.h" line="917"/>
        <location filename="ui_findwin.h" line="934"/>
        <location filename="ui_findwin.h" line="953"/>
        <source>Find what :</source>
        <translation type="unfinished">查找目标：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="105"/>
        <location filename="findwin.ui" line="381"/>
        <location filename="ui_findwin.h" line="918"/>
        <location filename="ui_findwin.h" line="936"/>
        <source>Backward direction</source>
        <translation type="unfinished">反向查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="112"/>
        <location filename="findwin.ui" line="388"/>
        <location filename="findwin.ui" line="717"/>
        <location filename="findwin.ui" line="1001"/>
        <location filename="ui_findwin.h" line="919"/>
        <location filename="ui_findwin.h" line="937"/>
        <location filename="ui_findwin.h" line="957"/>
        <location filename="ui_findwin.h" line="975"/>
        <source>Match whole word only</source>
        <translation type="unfinished">全词匹配</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="119"/>
        <location filename="findwin.ui" line="395"/>
        <location filename="findwin.ui" line="730"/>
        <location filename="findwin.ui" line="1008"/>
        <location filename="ui_findwin.h" line="920"/>
        <location filename="ui_findwin.h" line="938"/>
        <location filename="ui_findwin.h" line="958"/>
        <location filename="ui_findwin.h" line="976"/>
        <source>Match case</source>
        <translation type="unfinished">匹配大小写</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="126"/>
        <location filename="findwin.ui" line="402"/>
        <location filename="ui_findwin.h" line="921"/>
        <location filename="ui_findwin.h" line="939"/>
        <source>Wrap around</source>
        <translation type="unfinished">循环查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="151"/>
        <location filename="findwin.ui" line="427"/>
        <location filename="findwin.ui" line="756"/>
        <location filename="findwin.ui" line="1030"/>
        <location filename="ui_findwin.h" line="922"/>
        <location filename="ui_findwin.h" line="940"/>
        <location filename="ui_findwin.h" line="959"/>
        <location filename="ui_findwin.h" line="977"/>
        <source>Search Mode</source>
        <translation type="unfinished">查找模式</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="159"/>
        <location filename="findwin.ui" line="445"/>
        <location filename="findwin.ui" line="774"/>
        <location filename="findwin.ui" line="1038"/>
        <location filename="ui_findwin.h" line="923"/>
        <location filename="ui_findwin.h" line="942"/>
        <location filename="ui_findwin.h" line="961"/>
        <location filename="ui_findwin.h" line="978"/>
        <source>Regular expression</source>
        <translation type="unfinished">正则表达式</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="168"/>
        <location filename="findwin.ui" line="433"/>
        <location filename="findwin.ui" line="762"/>
        <location filename="findwin.ui" line="1047"/>
        <location filename="ui_findwin.h" line="924"/>
        <location filename="ui_findwin.h" line="941"/>
        <location filename="ui_findwin.h" line="960"/>
        <location filename="ui_findwin.h" line="979"/>
        <source>Normal</source>
        <translation type="unfinished">普通</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="178"/>
        <location filename="findwin.ui" line="454"/>
        <location filename="findwin.ui" line="783"/>
        <location filename="findwin.ui" line="1057"/>
        <location filename="ui_findwin.h" line="925"/>
        <location filename="ui_findwin.h" line="943"/>
        <location filename="ui_findwin.h" line="962"/>
        <location filename="ui_findwin.h" line="980"/>
        <source>Extend(\n,\r,\t,\0,\x...)</source>
        <translation type="unfinished">扩展(\n,\r,\t,\0,\x...)</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="200"/>
        <location filename="findwin.ui" line="476"/>
        <location filename="ui_findwin.h" line="926"/>
        <location filename="ui_findwin.h" line="944"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="225"/>
        <location filename="ui_findwin.h" line="927"/>
        <source>Find All in Current 
 Document</source>
        <translation type="unfinished">在当前文件中查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="251"/>
        <location filename="ui_findwin.h" line="929"/>
        <source>Find All in All Opened 
 Documents</source>
        <translation type="unfinished">查找所有打开文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="278"/>
        <location filename="findwin.ui" line="559"/>
        <location filename="findwin.ui" line="928"/>
        <location filename="findwin.ui" line="1109"/>
        <location filename="ui_findwin.h" line="932"/>
        <location filename="ui_findwin.h" line="949"/>
        <location filename="ui_findwin.h" line="972"/>
        <location filename="ui_findwin.h" line="983"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="301"/>
        <location filename="findwin.ui" line="495"/>
        <location filename="ui_findwin.h" line="945"/>
        <location filename="ui_findwin.h" line="950"/>
        <source>Replace</source>
        <translation type="unfinished">替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="340"/>
        <location filename="findwin.ui" line="653"/>
        <location filename="ui_findwin.h" line="935"/>
        <location filename="ui_findwin.h" line="954"/>
        <source>Replace with :</source>
        <translation type="unfinished">替换为：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="514"/>
        <location filename="ui_findwin.h" line="946"/>
        <source>Replace All</source>
        <translation type="unfinished">在文件中替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="539"/>
        <location filename="ui_findwin.h" line="947"/>
        <source>Replace All in All Opened 
 Documents</source>
        <translation type="unfinished">替换所有打开文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="902"/>
        <location filename="ui_findwin.h" line="970"/>
        <source>Replace In File</source>
        <translation type="unfinished">在文件中替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="259"/>
        <location filename="findwin.ui" line="915"/>
        <location filename="ui_findwin.h" line="931"/>
        <location filename="ui_findwin.h" line="971"/>
        <source>Clear Result</source>
        <translation type="unfinished">清空结果</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="951"/>
        <location filename="ui_findwin.h" line="984"/>
        <source>Mark</source>
        <translation type="unfinished">标记</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="961"/>
        <location filename="ui_findwin.h" line="974"/>
        <source>Mark What</source>
        <translation type="unfinished">标记目标：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="1077"/>
        <location filename="ui_findwin.h" line="981"/>
        <source>Mark All</source>
        <translation type="unfinished">全部标记</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="1090"/>
        <location filename="ui_findwin.h" line="982"/>
        <source>Clear Mark</source>
        <translation type="unfinished">清除</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="582"/>
        <location filename="ui_findwin.h" line="973"/>
        <source>Dir Find</source>
        <translation type="unfinished">在目录查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="592"/>
        <location filename="ui_findwin.h" line="951"/>
        <source>Dest Dir :</source>
        <translation type="unfinished">目标目录：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="615"/>
        <location filename="ui_findwin.h" line="952"/>
        <source>Select</source>
        <translation type="unfinished">选择</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="677"/>
        <location filename="ui_findwin.h" line="955"/>
        <source>File Type</source>
        <translation type="unfinished">文件类型：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="693"/>
        <location filename="ui_findwin.h" line="956"/>
        <source>*.c:*.cpp:*.h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="795"/>
        <location filename="ui_findwin.h" line="963"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="801"/>
        <location filename="ui_findwin.h" line="964"/>
        <source>Skip child dirs</source>
        <translation type="unfinished">跳过子目录</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="808"/>
        <location filename="ui_findwin.h" line="965"/>
        <source>Skip hide file</source>
        <translation type="unfinished">跳过隐藏文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="821"/>
        <location filename="ui_findwin.h" line="966"/>
        <source>Skip binary file</source>
        <translation type="unfinished">跳过二进制文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="836"/>
        <location filename="ui_findwin.h" line="967"/>
        <source>Skip Big file exceed</source>
        <translation type="unfinished">跳过超过大小的文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="859"/>
        <location filename="ui_findwin.h" line="968"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="889"/>
        <location filename="ui_findwin.h" line="969"/>
        <source>Find All</source>
        <translation type="unfinished">全部查找</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="690"/>
        <location filename="findwin.cpp" line="818"/>
        <location filename="findwin.cpp" line="909"/>
        <location filename="findwin.cpp" line="1050"/>
        <location filename="findwin.cpp" line="1144"/>
        <location filename="findwin.cpp" line="1214"/>
        <location filename="findwin.cpp" line="1325"/>
        <location filename="findwin.cpp" line="1882"/>
        <location filename="findwin.cpp" line="1979"/>
        <source>what find is null !</source>
        <translation type="unfinished">查找字段为空</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="720"/>
        <location filename="findwin.cpp" line="861"/>
        <location filename="findwin.cpp" line="1013"/>
        <location filename="findwin.cpp" line="1259"/>
        <location filename="findwin.cpp" line="1282"/>
        <location filename="findwin.cpp" line="1457"/>
        <source>cant&apos;t find text &apos;%1&apos;</source>
        <translation type="unfinished">找不到字段 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="738"/>
        <location filename="findwin.cpp" line="1032"/>
        <source>no more find text &apos;%1&apos;</source>
        <translation type="unfinished">没有更多的字段 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="152"/>
        <location filename="findwin.cpp" line="829"/>
        <source>The ReadOnly document does not allow this operation.</source>
        <translation type="unfinished">当前只读显示文件不允许该操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="894"/>
        <location filename="findwin.cpp" line="975"/>
        <source>find finished, total %1 found!</source>
        <translation type="unfinished">查找完成，一共 %1 处发现。</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="900"/>
        <source>The mode of the current document does not allow this operation.</source>
        <translation type="unfinished">当前模式下的文档不允许该操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1156"/>
        <location filename="findwin.cpp" line="1229"/>
        <source>The ReadOnly document does not allow replacement.</source>
        <translation type="unfinished">当前只读文档不允许执行替换操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1111"/>
        <location filename="findwin.cpp" line="1118"/>
        <source>no more replace text &apos;%1&apos;</source>
        <translation type="unfinished">没有更多替换文本 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="632"/>
        <source>find-regex-zero-length-match</source>
        <translation type="unfinished">正则查找零长匹配</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="672"/>
        <source>target info linenum %1 pos is %2 - %3</source>
        <translation type="unfinished">目标在行 %1 位置 %2 - %3</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1169"/>
        <location filename="findwin.cpp" line="1315"/>
        <source>The mode of the current document does not allow replacement.</source>
        <translation type="unfinished">当前模式的文档不允许执行替换操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1218"/>
        <source>Replace All current Doc</source>
        <translation type="unfinished">在所有打开文件中替换</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1218"/>
        <source>Are you sure replace all occurrences in current documents?</source>
        <translation type="unfinished">是否确认在当前打开的文档中替换？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1311"/>
        <source>replace finished, total %1 replaced!</source>
        <translation type="unfinished">替换完成，一共 %1 处替换！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1330"/>
        <source>Replace All Open Doc</source>
        <translation type="unfinished">替换所有打开的文档</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1330"/>
        <source>Are you sure replace all occurrences in all open documents?</source>
        <translation type="unfinished">是否确认在所有打开的文档中替换？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1415"/>
        <source>Replace in Opened Files: %1 occurrences were replaced.</source>
        <translation type="unfinished">在打开的文档中替换：%1 处已经被替换。</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1424"/>
        <source>what mark is null !</source>
        <translation type="unfinished">标记字段为空！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1467"/>
        <source>cant&apos;t mark text &apos;%1&apos;</source>
        <translation type="unfinished">不能标记文本 ‘%1’</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1510"/>
        <source>mark finished, total %1 found!</source>
        <translation type="unfinished">标记完成，一共 %1 处发现！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1514"/>
        <source>The mode of the current document does not allow mark.</source>
        <translation type="unfinished">当前模式的文档不允许执行标记操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1537"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1709"/>
        <source>load dir file in progress
, please wait ...</source>
        <translation type="unfinished">加载目录文件中，请等待...</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1746"/>
        <source>skip dir %1</source>
        <translation type="unfinished">跳过目录 %1</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1756"/>
        <source>found %1 dir %2</source>
        <translation type="unfinished">发现 %1 个目录 %2</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1794"/>
        <source>ext type  skip file %1</source>
        <translation type="unfinished">跳过类型文件 %1</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1820"/>
        <source>found in dir canceled ...</source>
        <translation type="unfinished">查找取消...</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1828"/>
        <source>Continue Find ?</source>
        <translation type="unfinished">是否继续查找？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1828"/>
        <source>The search results have been greater than %1 times in %2 files, and more may be slow. Continue to search?</source>
        <translation type="unfinished">查找结果已经有 %1 处在 %2 个文件中，结果太多会比较慢，是否继续查找？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1828"/>
        <source>Yes</source>
        <translation type="unfinished">继续查找</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1828"/>
        <source>Abort</source>
        <translation type="unfinished">终止查找</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1875"/>
        <location filename="findwin.cpp" line="1972"/>
        <source>please select find dest dir !</source>
        <translation type="unfinished">请选择目标文件夹！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1890"/>
        <source>dest dir %1 not exist !</source>
        <translation type="unfinished">目标文件夹 %1 不存在！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1946"/>
        <source>find finished, total %1 found in %2 file!</source>
        <translation type="unfinished">查找完成，一共发现 %1 处在 %2 个文件中！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1984"/>
        <source>Replace All Dirs</source>
        <translation type="unfinished">目录全部替换</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1984"/>
        <source>Are you sure replace all &quot;%1&quot; to &quot;%2&quot; occurrences in selected dirs ?</source>
        <translation type="unfinished">您确定替换目录文件中所有 &quot;%1&quot; 为 &quot;%2&quot; 吗？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2038"/>
        <source>replace finished, total %1 replace in %2 file!</source>
        <translation type="unfinished">替换完成，一共替换 %1 处在 %2 个文件中！</translation>
    </message>
</context>
<context>
    <name>GoToLineWin</name>
    <message>
        <location filename="gotolinewin.ui" line="20"/>
        <location filename="ui_gotolinewin.h" line="125"/>
        <source>GoToLineWin</source>
        <translation type="unfinished">跳转到行</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="50"/>
        <location filename="ui_gotolinewin.h" line="126"/>
        <source>Line Num</source>
        <translation type="unfinished">行号</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="71"/>
        <location filename="ui_gotolinewin.h" line="127"/>
        <source>Left</source>
        <translation type="unfinished">左边</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="81"/>
        <location filename="ui_gotolinewin.h" line="128"/>
        <source>Right</source>
        <translation type="unfinished">右边</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="108"/>
        <location filename="ui_gotolinewin.h" line="129"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="115"/>
        <location filename="ui_gotolinewin.h" line="130"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>HexCmpRangeWin</name>
    <message>
        <location filename="hexcmprangewin.ui" line="20"/>
        <location filename="ui_hexcmprangewin.h" line="171"/>
        <source>HexCmpRangeWin</source>
        <translation type="unfinished">选择文件对比范围</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="32"/>
        <location filename="ui_hexcmprangewin.h" line="172"/>
        <source>Max Bin File Size is 10M ! Exceeding file size ! 
Select a shorter range for comparison. </source>
        <translation type="unfinished">文件最大对比长度为10M! 当前文件超过最大限制。
请选择一个文件范围来进行对比。</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="40"/>
        <location filename="ui_hexcmprangewin.h" line="174"/>
        <source>Select Range</source>
        <translation type="unfinished">选择对比范围</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="48"/>
        <location filename="ui_hexcmprangewin.h" line="175"/>
        <source>Left Start Pos: </source>
        <translation type="unfinished">左边开始偏移值：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="61"/>
        <location filename="hexcmprangewin.ui" line="108"/>
        <location filename="ui_hexcmprangewin.h" line="176"/>
        <location filename="ui_hexcmprangewin.h" line="180"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="68"/>
        <location filename="ui_hexcmprangewin.h" line="177"/>
        <source>Left Compare Length:</source>
        <translation type="unfinished">左边对比长度：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="81"/>
        <location filename="hexcmprangewin.ui" line="128"/>
        <location filename="ui_hexcmprangewin.h" line="178"/>
        <location filename="ui_hexcmprangewin.h" line="182"/>
        <source>10240</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="95"/>
        <location filename="ui_hexcmprangewin.h" line="179"/>
        <source>Right Start Pos:</source>
        <translation type="unfinished">右边开始偏移值：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="115"/>
        <location filename="ui_hexcmprangewin.h" line="181"/>
        <source>Right Compare Length:</source>
        <translation type="unfinished">右边对比长度：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="165"/>
        <location filename="ui_hexcmprangewin.h" line="184"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="172"/>
        <location filename="ui_hexcmprangewin.h" line="185"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.cpp" line="69"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.cpp" line="69"/>
        <source>StartPos or cmpare lens value error.</source>
        <translation type="unfinished">开始位置或对比长度值错误！</translation>
    </message>
</context>
<context>
    <name>HexFileGoto</name>
    <message>
        <location filename="hexfilegoto.ui" line="20"/>
        <location filename="ui_hexfilegoto.h" line="148"/>
        <source>HexFileGoto</source>
        <translation type="unfinished">二进制跳转</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="45"/>
        <location filename="ui_hexfilegoto.h" line="149"/>
        <source>Addr</source>
        <translation type="unfinished">地址</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="68"/>
        <location filename="ui_hexfilegoto.h" line="150"/>
        <source>Dec Addr</source>
        <translation type="unfinished">10进制地址</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="89"/>
        <location filename="ui_hexfilegoto.h" line="151"/>
        <source>Hex Addr</source>
        <translation type="unfinished">16进制地址</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="142"/>
        <location filename="ui_hexfilegoto.h" line="152"/>
        <source>Go to</source>
        <translation type="unfinished">跳转</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="149"/>
        <location filename="ui_hexfilegoto.h" line="153"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>HexRuleWin</name>
    <message>
        <location filename="hexrulewin.ui" line="20"/>
        <location filename="ui_hexrulewin.h" line="118"/>
        <source>HexRuleWinRule</source>
        <translation type="unfinished">bin对比规则</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="30"/>
        <location filename="ui_hexrulewin.h" line="119"/>
        <source>Mode</source>
        <translation type="unfinished">模式</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="36"/>
        <location filename="ui_hexrulewin.h" line="120"/>
        <source>Maximum Common String</source>
        <translation type="unfinished">最大公共内容方式</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="46"/>
        <location filename="ui_hexrulewin.h" line="121"/>
        <source>One-to-one Byte Contrast</source>
        <translation type="unfinished">按字节一对一比较</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="56"/>
        <location filename="ui_hexrulewin.h" line="122"/>
        <source>Highlight diff Background</source>
        <translation type="unfinished">高亮不同处背景</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="91"/>
        <location filename="ui_hexrulewin.h" line="123"/>
        <source>OK</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="98"/>
        <location filename="ui_hexrulewin.h" line="124"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>NetRegister</name>
    <message>
        <source>notice</source>
        <translation type="obsolete">消息</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="122"/>
        <source>Font Lost</source>
        <translation type="unfinished">字体缺失</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="122"/>
        <source>Please Install System Font [Courier/SimSun/Times New Roman].The interface font will display exceptions</source>
        <translation type="unfinished">请安装至少一种系统字体[[Courier/SimSun/Times New Roman]。软件界面字体显示可能不满意。</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="246"/>
        <location filename="netregister.cpp" line="250"/>
        <location filename="netregister.cpp" line="263"/>
        <source>The new software has been released, please update it timely!</source>
        <translation type="unfinished">软件新版本已经发布，请及时更新！</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="225"/>
        <location filename="netregister.cpp" line="280"/>
        <source>Software status is normal.</source>
        <translation type="unfinished">软件状态正常</translation>
    </message>
    <message>
        <source>&lt;a href = &quot;www.itdp.cn&quot;&gt;&lt;u&gt;New Version Was Detected&lt;/u&gt;&lt;/a&gt;</source>
        <translation type="obsolete">&lt;a href = &quot;www.itdp.cn&quot;&gt;&lt;u&gt;发现新版本...&lt;/u&gt;&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1 not exist, please check!</source>
        <translation type="obsolete">文件 %1 不存在，请检查！</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="obsolete">消息</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="347"/>
        <source>Please contact us. QQ Group:959439826</source>
        <translation type="unfinished">请加入我们QQ群：959439826</translation>
    </message>
    <message>
        <source>Please contact us. QQ:959439826</source>
        <translation type="obsolete">请联系我们QQ群：959439826</translation>
    </message>
    <message>
        <source>Close ?</source>
        <translation type="obsolete">关闭?</translation>
    </message>
    <message>
        <source>already has child window open, close all ?</source>
        <translation type="obsolete">目前还有子窗口处于打开状态，关闭所有窗口吗？</translation>
    </message>
    <message>
        <source>file path not exist, remove recent record!</source>
        <translation type="obsolete">文件路径不存在，删除历史记录！</translation>
    </message>
    <message>
        <source>file [%1] may be not a text file, cmp in hex mode?</source>
        <translation type="obsolete">文件 %1 可能不是文本格式，是否进行二进制格式对比？</translation>
    </message>
</context>
<context>
    <name>OptionsView</name>
    <message>
        <location filename="optionsview.ui" line="14"/>
        <location filename="ui_optionsview.h" line="97"/>
        <source>OptionsView</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <location filename="optionsview.ui" line="24"/>
        <location filename="ui_optionsview.h" line="98"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="optionsview.ui" line="63"/>
        <location filename="ui_optionsview.h" line="99"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>file correlation</source>
        <translation type="obsolete">文件关联</translation>
    </message>
    <message>
        <location filename="optionsview.cpp" line="17"/>
        <source>File Correlation</source>
        <translation type="unfinished">文件关联</translation>
    </message>
    <message>
        <location filename="optionsview.cpp" line="23"/>
        <source>Compare File Types</source>
        <translation type="unfinished">对比文件类型</translation>
    </message>
    <message>
        <location filename="optionsview.cpp" line="28"/>
        <source>Text And Fonts</source>
        <translation type="unfinished">文本和字体</translation>
    </message>
</context>
<context>
    <name>ProgressWin</name>
    <message>
        <location filename="progresswin.ui" line="17"/>
        <location filename="ui_progresswin.h" line="104"/>
        <source>work progress</source>
        <translation type="unfinished">工作进度</translation>
    </message>
    <message>
        <location filename="progresswin.ui" line="36"/>
        <location filename="ui_progresswin.h" line="105"/>
        <source>current progress</source>
        <translation type="unfinished">当前进度</translation>
    </message>
    <message>
        <location filename="progresswin.ui" line="70"/>
        <location filename="ui_progresswin.h" line="106"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <location filename="progresswin.cpp" line="67"/>
        <source>Notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="progresswin.cpp" line="67"/>
        <source>Are you sure to cancel?</source>
        <translation type="unfinished">您确定取消吗？</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="108"/>
        <source>Text Mode</source>
        <translation type="unfinished">文本模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="111"/>
        <source>Hex ReadOnly Mode</source>
        <translation type="unfinished">二进制只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="114"/>
        <source>Bit Text ReadOnly Mode</source>
        <translation type="unfinished">大文本只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="117"/>
        <source>Text ReadOnly Mode</source>
        <translation type="unfinished">文本只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="120"/>
        <source>File Mode</source>
        <translation type="unfinished">文件模式</translation>
    </message>
</context>
<context>
    <name>QsciDisplayWindow</name>
    <message>
        <location filename="qscidisplaywindow.cpp" line="325"/>
        <source>Find Text</source>
        <translation type="unfinished">查找文本</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="326"/>
        <source>Show File in Explorer</source>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="392"/>
        <location filename="qscidisplaywindow.cpp" line="412"/>
        <source>Not Find</source>
        <translation type="unfinished">没有找到</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="392"/>
        <source>Not Find Next!</source>
        <translation type="unfinished">找不到下一个！</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="412"/>
        <source>Not Find Prev!</source>
        <translation type="unfinished">找不到前一个！</translation>
    </message>
</context>
<context>
    <name>QsciLexerText</name>
    <message>
        <source>Chinese And Others</source>
        <translation type="unfinished">中文字符及其它</translation>
    </message>
    <message>
        <source>Ascii</source>
        <translation type="unfinished">英文字符</translation>
    </message>
</context>
<context>
    <name>QsciScintilla</name>
    <message>
        <source>&amp;Cut</source>
        <translation type="unfinished">剪切</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="unfinished">复制</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="unfinished">粘贴</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished">删除</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="unfinished">全选</translation>
    </message>
</context>
<context>
    <name>QtLangSet</name>
    <message>
        <location filename="qtlangset.cpp" line="189"/>
        <source>Save Change</source>
        <translation type="unfinished">保存修改</translation>
    </message>
    <message>
        <source>The current style configuration has been modified. Do you want to save it?</source>
        <translation type="obsolete">当前语言的格式风格已经被修改，是否保存？</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="189"/>
        <source>%1 style configuration has been modified. Do you want to save it?</source>
        <translation type="unfinished">%1 类型的显示风格已经被修改，是否保存？</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="341"/>
        <source>Style Foreground Color</source>
        <translatorcomment>风格背景色</translatorcomment>
        <translation type="unfinished">风格前景色</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="366"/>
        <source>Style Background Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtLangSetClass</name>
    <message>
        <location filename="qtlangset.ui" line="14"/>
        <location filename="ui_qtlangset.h" line="315"/>
        <source>QtLangSet</source>
        <translation type="unfinished">编程语言风格设置</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="29"/>
        <location filename="ui_qtlangset.h" line="316"/>
        <source>Language</source>
        <translation type="unfinished">语言</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="56"/>
        <location filename="ui_qtlangset.h" line="317"/>
        <source>Style</source>
        <translation type="unfinished">皮肤风格</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="87"/>
        <location filename="ui_qtlangset.h" line="318"/>
        <source>Color</source>
        <translation type="unfinished">颜色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="95"/>
        <location filename="ui_qtlangset.h" line="319"/>
        <source>Foreground:</source>
        <translation type="unfinished">前景色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="114"/>
        <location filename="ui_qtlangset.h" line="320"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="121"/>
        <location filename="ui_qtlangset.h" line="321"/>
        <source>Select</source>
        <translation type="unfinished">选择</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="145"/>
        <location filename="ui_qtlangset.h" line="322"/>
        <source>background:</source>
        <translation type="unfinished">背景色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="164"/>
        <location filename="ui_qtlangset.h" line="323"/>
        <source>Same As Theme</source>
        <translation type="unfinished">与主题保存一致</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="189"/>
        <location filename="ui_qtlangset.h" line="324"/>
        <source>Font</source>
        <translation type="unfinished">字体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="197"/>
        <location filename="ui_qtlangset.h" line="325"/>
        <source>Font Size:</source>
        <translation type="unfinished">字体大小</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="211"/>
        <location filename="ui_qtlangset.h" line="326"/>
        <source>Bold</source>
        <translation type="unfinished">粗体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="218"/>
        <location filename="ui_qtlangset.h" line="327"/>
        <source>Italic</source>
        <translation type="unfinished">斜体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="225"/>
        <location filename="ui_qtlangset.h" line="328"/>
        <source>Underline</source>
        <translation type="unfinished">下划线</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="236"/>
        <location filename="ui_qtlangset.h" line="329"/>
        <source>Name:</source>
        <translation type="unfinished">名称：</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="268"/>
        <location filename="ui_qtlangset.h" line="330"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="275"/>
        <location filename="ui_qtlangset.h" line="331"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>RcTreeWidget</name>
    <message>
        <location filename="RcTreeWidget.cpp" line="47"/>
        <source>Show File in Explorer</source>
        <oldsource>Show File in Explorer...</oldsource>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
</context>
<context>
    <name>ReNameWin</name>
    <message>
        <location filename="renamewin.ui" line="20"/>
        <location filename="ui_renamewin.h" line="273"/>
        <source>ReNameBatchWin</source>
        <translation type="unfinished">批量重命名</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="32"/>
        <location filename="ui_renamewin.h" line="274"/>
        <source>Select Dir</source>
        <translation type="unfinished">选择目录</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="42"/>
        <location filename="ui_renamewin.h" line="275"/>
        <source>Browse</source>
        <translation type="unfinished">浏览</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="55"/>
        <location filename="ui_renamewin.h" line="282"/>
        <source>AddDelNameString</source>
        <translation type="unfinished">增加删除</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="61"/>
        <location filename="ui_renamewin.h" line="276"/>
        <source>Add Prefix</source>
        <translation type="unfinished">增加前缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="75"/>
        <location filename="ui_renamewin.h" line="277"/>
        <source>Del Prefix</source>
        <translation type="unfinished">删除前缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="89"/>
        <location filename="ui_renamewin.h" line="278"/>
        <source>Add Suffix</source>
        <translation type="unfinished">增加后缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="103"/>
        <location filename="ui_renamewin.h" line="279"/>
        <source>Del Suffix</source>
        <translation type="unfinished">删除后缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="117"/>
        <location filename="ui_renamewin.h" line="280"/>
        <source>Lower FileName</source>
        <translation type="unfinished">小写文件名</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="124"/>
        <location filename="ui_renamewin.h" line="281"/>
        <source>Upper FileName</source>
        <translation type="unfinished">大写文件名</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="132"/>
        <location filename="ui_renamewin.h" line="288"/>
        <source>ChangeExt</source>
        <translation type="unfinished">改变后缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="140"/>
        <location filename="ui_renamewin.h" line="283"/>
        <source>Modify Ext To</source>
        <translation type="unfinished">修改文件后缀为</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="167"/>
        <location filename="ui_renamewin.h" line="284"/>
        <source>Deal Ext Type</source>
        <translation type="unfinished">处理的后缀类型</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="181"/>
        <location filename="ui_renamewin.h" line="285"/>
        <source>All File Ext Type</source>
        <translation type="unfinished">所有支持的类型</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="189"/>
        <location filename="ui_renamewin.h" line="287"/>
        <source>defined</source>
        <translation type="unfinished">自定义</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="228"/>
        <location filename="ui_renamewin.h" line="289"/>
        <source>Deal Child Dir</source>
        <translation type="unfinished">处理子目录</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="250"/>
        <location filename="ui_renamewin.h" line="290"/>
        <source>Start</source>
        <translation type="unfinished">开始</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="257"/>
        <location filename="ui_renamewin.h" line="291"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="32"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="66"/>
        <source>input file ext()</source>
        <translation type="unfinished">输入文件后缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="66"/>
        <source>ext (Start With .)</source>
        <translation type="unfinished">后缀（以.开头）</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="97"/>
        <location filename="renamewin.cpp" line="107"/>
        <location filename="renamewin.cpp" line="119"/>
        <location filename="renamewin.cpp" line="131"/>
        <location filename="renamewin.cpp" line="143"/>
        <location filename="renamewin.cpp" line="166"/>
        <location filename="renamewin.cpp" line="306"/>
        <location filename="renamewin.cpp" line="322"/>
        <location filename="renamewin.cpp" line="458"/>
        <source>Notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="97"/>
        <source>Please Select Dir</source>
        <translation type="unfinished">请选择目录</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="107"/>
        <source>Please Input Add File Prefix</source>
        <translation type="unfinished">请选择增加的文件前缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="119"/>
        <source>Please Input Del File Prefix</source>
        <translation type="unfinished">请选择删除的文件前缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="131"/>
        <source>Please Input Add File Suffix</source>
        <translation type="unfinished">请选择增加的文件后缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="143"/>
        <source>Please Input Del File Suffix</source>
        <translation type="unfinished">请选择删除的文件后缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="166"/>
        <source>Please Select One Operator</source>
        <translation type="unfinished">请选择一个操作</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="229"/>
        <location filename="renamewin.cpp" line="372"/>
        <source>rename file in progress, please wait ...</source>
        <translation type="unfinished">正在重命名文件中，请等待...</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="293"/>
        <location filename="renamewin.cpp" line="444"/>
        <source>failed %1 file path %2, please check</source>
        <translation type="unfinished">失败 %1 个，文件路径 %2 请检查</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="306"/>
        <location filename="renamewin.cpp" line="458"/>
        <source>Deal Finished, totol %1 files, failed %2 files</source>
        <translation type="unfinished">处理完成，一共 %1 个文件，失败 %2 个文件。</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="322"/>
        <source>Please Select Dir Or Dest Ext</source>
        <translation type="unfinished">请选择目录或目标文件后缀</translation>
    </message>
</context>
<context>
    <name>RealCompareClass</name>
    <message>
        <source>MainPage</source>
        <translation type="obsolete">主页</translation>
    </message>
    <message>
        <source>dir compare</source>
        <translation type="obsolete">目录对比</translation>
    </message>
    <message>
        <source>file compare</source>
        <translation type="obsolete">文件对比</translation>
    </message>
    <message>
        <source>encode convert</source>
        <translation type="obsolete">编码转换</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>donate</source>
        <translation type="obsolete">捐赠软件</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">语言</translation>
    </message>
    <message>
        <source>bin compare</source>
        <translation type="obsolete">二进制对比</translation>
    </message>
    <message>
        <source>Recently</source>
        <translation type="obsolete">最近对比</translation>
    </message>
    <message>
        <source>Dir ...</source>
        <translation type="obsolete">目录...</translation>
    </message>
    <message>
        <source>File ...</source>
        <translation type="obsolete">文件...</translation>
    </message>
    <message>
        <source>dir</source>
        <translation type="obsolete">目录</translation>
    </message>
    <message>
        <source>file</source>
        <translation type="obsolete">文件</translation>
    </message>
    <message>
        <source>rule</source>
        <translation type="obsolete">规则</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">英文</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="obsolete">中文</translation>
    </message>
    <message>
        <source>main</source>
        <translation type="obsolete">主页</translation>
    </message>
</context>
<context>
    <name>StatusWidget</name>
    <message>
        <location filename="statuswidget.ui" line="32"/>
        <location filename="ui_statuswidget.h" line="51"/>
        <source>notice msg</source>
        <translation type="unfinished">通知消息</translation>
    </message>
</context>
<context>
    <name>TextEditSetWin</name>
    <message>
        <location filename="texteditsetwin.ui" line="14"/>
        <location filename="ui_texteditsetwin.h" line="159"/>
        <source>TextEditSetWin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="25"/>
        <location filename="ui_texteditsetwin.h" line="160"/>
        <source>Tab Setting</source>
        <translation type="unfinished">Tab 设置</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="33"/>
        <location filename="ui_texteditsetwin.h" line="161"/>
        <source>Set Tab Length</source>
        <translation type="unfinished">Tab 字符长度</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="52"/>
        <location filename="ui_texteditsetwin.h" line="162"/>
        <source>Space Replacement Tab</source>
        <translation type="unfinished">使用空格替换tab</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="65"/>
        <location filename="ui_texteditsetwin.h" line="163"/>
        <source>Big Text Size</source>
        <translation type="unfinished">大文本文件大小</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="71"/>
        <location filename="ui_texteditsetwin.h" line="164"/>
        <source>Beyond this size, it can only be read-only and displayed in blocks</source>
        <translation type="unfinished">文本文件超过该值时，只能以只读的方式进行分块加载显示。</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="80"/>
        <location filename="ui_texteditsetwin.h" line="165"/>
        <source>Exceed(MB)</source>
        <translation type="unfinished">超过(MB)</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="100"/>
        <location filename="ui_texteditsetwin.h" line="166"/>
        <source>(50-300MB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="140"/>
        <location filename="ui_texteditsetwin.h" line="167"/>
        <source>Remember files opened on close</source>
        <translation type="unfinished">记住最后打开的文件</translation>
    </message>
    <message>
        <source>Txt Font Set（Only Text File)</source>
        <oldsource>Txt Font Set</oldsource>
        <translation type="obsolete">文本默认字体设置（只对TXT文本有效）</translation>
    </message>
    <message>
        <source>Programming Language Default Font</source>
        <translation type="obsolete">编程语言默认字体</translation>
    </message>
    <message>
        <source>Txt Font Set(Only Windows)</source>
        <translation type="obsolete">文本默认字体设置</translation>
    </message>
    <message>
        <source>Default Font</source>
        <translation type="obsolete">默认字体</translation>
    </message>
    <message>
        <source>Select Font</source>
        <translation type="obsolete">选择字体</translation>
    </message>
    <message>
        <location filename="texteditsetwin.cpp" line="100"/>
        <location filename="texteditsetwin.cpp" line="133"/>
        <source>User define Txt Font</source>
        <translation type="unfinished">用户自定义文本字体</translation>
    </message>
</context>
<context>
    <name>TextFind</name>
    <message>
        <location filename="textfind.ui" line="14"/>
        <location filename="ui_textfind.h" line="108"/>
        <source>TextFind</source>
        <translation type="unfinished">查找文本</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="24"/>
        <location filename="ui_textfind.h" line="109"/>
        <source>Find Options</source>
        <translation type="unfinished">查找选项</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="32"/>
        <location filename="ui_textfind.h" line="110"/>
        <source>Find Text</source>
        <translation type="unfinished">查找文本</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="49"/>
        <location filename="ui_textfind.h" line="111"/>
        <source>Find Prev</source>
        <translation type="unfinished">查找前一个</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="56"/>
        <location filename="ui_textfind.h" line="112"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="63"/>
        <location filename="ui_textfind.h" line="113"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>UserRegister</name>
    <message>
        <location filename="userregister.cpp" line="19"/>
        <source>Free Trial</source>
        <translation type="unfinished">免费永久试用版本（没有捐赠我们）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="23"/>
        <location filename="userregister.cpp" line="56"/>
        <source>Registered Version</source>
        <translation type="unfinished">注册过的正版软件！（恭喜）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="27"/>
        <source>License Expired</source>
        <translation type="unfinished">许可证过期（可捐赠获取）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="31"/>
        <source>License Error</source>
        <translation type="unfinished">错误的注册码（可重新输入）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="71"/>
        <location filename="userregister.cpp" line="77"/>
        <location filename="userregister.cpp" line="84"/>
        <location filename="userregister.cpp" line="107"/>
        <location filename="userregister.cpp" line="111"/>
        <location filename="userregister.cpp" line="116"/>
        <source>Licence Key</source>
        <translation type="unfinished">许可证</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="71"/>
        <source>It is already a registered version.</source>
        <translation type="unfinished">当前已经是注册版！</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="77"/>
        <source>Please scanning the donation, Write your email address in the message area.
You will get the registration code!</source>
        <translation type="unfinished">请微信扫描捐赠，在留言处留下您的邮件地址，注册码将通过邮件发送给您。</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="84"/>
        <source>Please enter the correct registration code!</source>
        <translation type="unfinished">请输入正确的注册码！</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="107"/>
        <source>Congratulations on your successful registration.</source>
        <translation type="unfinished">恭喜注册成功！</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="111"/>
        <location filename="userregister.cpp" line="116"/>
        <source>Registration failed. Please try again later.</source>
        <translation type="unfinished">注册失败，请稍后再试！</translation>
    </message>
    <message>
        <source>Please scanning the donation, get and enter the correct code !</source>
        <translation type="obsolete">请微信扫码赞赏后，输入转账单号后11位到许可序列码中。</translation>
    </message>
    <message>
        <source>Please check code length , Only the last 11 digits are required !</source>
        <translation type="obsolete">请检查许可序列长度，只需要后11位即可。</translation>
    </message>
    <message>
        <source>Processing succeeded. We will process your registration code in the background. It may take 1-3 days.</source>
        <translation type="obsolete">处理成功。我们将在1-3天内处理您的许可注册码。</translation>
    </message>
</context>
<context>
    <name>UserRegisterClass</name>
    <message>
        <location filename="userregister.ui" line="20"/>
        <location filename="ui_userregister.h" line="191"/>
        <source>UserRegister</source>
        <translation type="unfinished">捐赠获取注册码</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="115"/>
        <location filename="userregister.ui" line="121"/>
        <location filename="ui_userregister.h" line="194"/>
        <location filename="ui_userregister.h" line="195"/>
        <source>Status</source>
        <translation type="unfinished">软件状态</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="138"/>
        <location filename="ui_userregister.h" line="196"/>
        <source>Machine Id</source>
        <translation type="unfinished">机器码</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="155"/>
        <location filename="ui_userregister.h" line="197"/>
        <source>Licence Key</source>
        <translation type="unfinished">序列号</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="165"/>
        <location filename="ui_userregister.h" line="198"/>
        <source>22874567148</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userregister.ui" line="177"/>
        <location filename="ui_userregister.h" line="199"/>
        <source>Register</source>
        <translation type="unfinished">注册</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="184"/>
        <location filename="ui_userregister.h" line="200"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="193"/>
        <location filename="ui_userregister.h" line="201"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wechat scans the appreciation code on the left.&lt;/p&gt;&lt;p&gt;Leave your contact email in the donation message.&lt;/p&gt;&lt;p&gt;We will send you the registration code later.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">请微信扫码赞赏软件。
在留言处留下您的邮件地址，注册码将通过邮件发送给您。
在序列号处输入您得到的注册码，点击注册获取注册版软件
您可以免费使用该软件，您也可以捐赠我们获取注册版。</translation>
    </message>
</context>
<context>
    <name>closeDlg</name>
    <message>
        <location filename="closeDlg.ui" line="26"/>
        <location filename="ui_closeDlg.h" line="150"/>
        <source>closeDlg</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="56"/>
        <location filename="ui_closeDlg.h" line="151"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">是否保存修改？</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="63"/>
        <location filename="ui_closeDlg.h" line="152"/>
        <source>save left document ?</source>
        <translation type="unfinished">保存左边文档？</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="73"/>
        <location filename="ui_closeDlg.h" line="153"/>
        <source>save right document ?</source>
        <translation type="unfinished">保存右边文档？</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="143"/>
        <location filename="ui_closeDlg.h" line="154"/>
        <source>save selected</source>
        <translation type="unfinished">保存所选文档</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="162"/>
        <location filename="ui_closeDlg.h" line="155"/>
        <source>discard</source>
        <translation type="unfinished">放弃修改</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="181"/>
        <location filename="ui_closeDlg.h" line="156"/>
        <source>cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>donate</name>
    <message>
        <location filename="donate.ui" line="29"/>
        <location filename="ui_donate.h" line="114"/>
        <source>donate</source>
        <translation type="unfinished">捐赠作者</translation>
    </message>
    <message>
        <location filename="donate.ui" line="81"/>
        <location filename="ui_donate.h" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donation Software Development By WeChat &lt;/p&gt;&lt;p&gt;Busy living, no time to improve software&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">现实生活中的一地鸡毛
让我无法投入更多时间完善免费软件
请通过微信扫码捐赠作者</translation>
    </message>
    <message>
        <source>Donation Software Development By WeChat </source>
        <translation type="obsolete">通过微信捐赠CC对比软件发展
感谢支持国产软件！</translation>
    </message>
</context>
</TS>
